isRunning = false;
$(document).ajaxError(function(event, request, settings, error) {
    if (isRunning == false) {
        isRunning = true;
        loadingOcultar();

        switch (request.status) {
        case 401:
            window.location.href = window.baseUrl.concat('/');
            break;
        case 419:
            $("#form-modal-login").trigger("reset");
            $("#modal-login-error").hide();
            $("#modal-login").modal({ 
                show: true,
                backdrop: true,
                focus: true,
                keyboard: false
            });
            break;
        default:
            console.log(request, settings, error);

            modalAlert({
                title: 'Erro na abertura do modal de login!',
                body: 'ERROR ' + request.status + ': ' + error
            });

        }
        loadingOcultar();
        isRunning = false;
    }
});