$(document).ready(function () {
    loadingScripts();
    loadingOcultar();
    openSidebar();
});

$(document).ajaxSuccess(function(event, request, settings) {
    if (typeof request.responseJSON.dateHour != 'undefined') {
        var datehour = request.responseJSON.dateHour.split(" ");
        $('#date-display').html(datehour[0]);
        $('#hour-display').html(datehour[1]);
    }
});

/**
 * Evento de loadingScripts de paginas em ajax
 */
$(document).on('click', '.loading-ajax', function () {
    loadingExibir();

    //recupera url do atributo data do elemento
    let urlAction = $(this).attr('data-url');
    let modal = $(this).attr('data-modal');
    let div = $(this).attr('data-div');

    setTimeout(function () {
        //chama função responsavel por fazer a solicitação
        loadingAjax({
            urlAction: urlAction,
            modal: modal,
            div: div
        });

    }, 500);
});

/**
 * Evento de submit de um form que contenha a classe "form-submit-default"
 * Este evento é o padrão do sistema
 */
 $(document).on('submit', '.form-submit-default', function (e) {
    loadingExibir();
    e.preventDefault();

    let idForm = $(this).attr("id");
    let urlAction = $(this).attr("data-url");
    let urlRedirect = $(this).attr("data-redirect");
    let div = $(this).attr("data-div");

    setTimeout(function () {

        submitForm({
            idForm: idForm,
            urlAction: urlAction,
            urlRedirect: urlRedirect,
            div: div
        });

    }, 500);
});

/**
 * Chama a função responsavel por formatar os valoes
 */
 $(document).on('keyup','.decimal', function(e){
    let tamanho = 20;
    let decimal = 2;
    if(typeof ($(this).attr('data-tamanho')) !== 'undefined'){
        tamanho = $(this).attr('data-tamanho');
    }

    if(typeof ($(this).attr('data-decimal')) !== 'undefined'){
        decimal = $(this).attr('data-decimal');
    }

    decimalBr(this, tamanho, decimal);
});

/**
 * Para deletar registros
 */
 $(document).on('click', '.delete-ajax', function(){
    loadingExibir();

    let idForm = 'form-modal-corfirm';
    let urlAction = $(this).attr("data-url");
    let urlRedirect = $(this).attr("data-redirect");
    let idKey = $(this).attr("data-key");
    let modal = $(this).attr("data-modal");

    setTimeout(function () {
        corpo = `<div class='form-group'>
                    <div class='col-sm-12'>
                        <input name='idKey' value='${idKey}' type='hidden'>
                        <label for='id${idKey}'>Registro id: "${idKey}"</label>
                    </div>
                </div>`;

        //verifica se o id é realmente um numero
        if (isNaN(idKey)) {
            body = "A chave do item não é um numero valido."
            //chama o modal de confirmação
            modalConfirm({
                body: corpo,
                title: "Tem certeza que deseja excluir o item?",
                danger: true,
                disabled: true
            });
            return;
        }

        //chama o modal de confirmação
        modalConfirm({
            body: corpo,
            title: "Tem certeza que deseja excluir o item?",
            labelOk: "Remover",
            urlAction: urlAction,
            urlRedirect: urlRedirect,
            danger: false,
            callback: function() {
                loadingExibir();
                setTimeout(function () {

                    submitForm({
                        idForm: idForm,
                        urlAction: urlAction,
                        urlRedirect: urlRedirect
                    });

                }, 500);
            }
        });

        loadingOcultar();
    }, 500);
});


$(document).on('change', '.loading-orcamento', function (event){

    let urlAction = window.baseUrl.concat("/orcamento-cliente?idCliente=").concat($(this).val());
  
    loadingAjax({
        urlAction: urlAction,
        modal: false,
        div: "card-datatable"
    });

});

$(document).on('change', '.loading-orcamento-cliente', function (event){

    let vetor = $(this).val();
    let formData = new FormData();

    if (vetor.length == 0) {
        let selectbox = $("#idPedido");
        selectbox.find('option').remove();
        return;
    }

    for (var i in vetor) {
        formData.append('chave[]', vetor[i]);
    }

    executeScriptAjax({
        urlAction: window.baseUrl.concat("/orcamento-cliente-usuaro"),
        method: "POST",
        data: formData,
        callback: function (response) {
            let selectbox = $("#idPedido");
            let pedido = $("#idPedido").val();

            selectbox.find('option').remove();
            $.each(response.item, function (i, d) {

                let selected = '';
                if (typeof pedido.find(element => element == `${d.cli_codcliente}-${d.orc_codorcamento}`) != 'undefined') {
                    selected = 'selected';
                }

                selectbox.append(`<option value="${d.cli_codcliente}-${d.orc_codorcamento}" ${selected}>${d.orc_codorcamento}</option>`);
            });

            loadingScripts();
            loadingOcultar();
        }
    });

});


/**
 * Evento para abrir modal em cima de modal
 */
$(document).on('show.bs.modal', '.modal', function (event) {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

/**
 * Função para acionar a filtragem do menu sidebar
 */
$(document).on('keyup', '#search-sidebar', function() {
    var input, li, count = 0;
    input = $(this).val().trim().toLowerCase();
    li = $("#my-search li");

    setTimeout(function() {

        $(li).each(function(i, elem) {
            let a = $(elem).find('a');
            if($(a).text().trim().toLowerCase().indexOf(input) > -1 && input.length >= 3){
                count++;

                $("#search-not").removeClass('display-on display-off');
                $("#search-not").addClass('display-off');

                $(elem).removeClass('display-off');
                $(elem).addClass('display-on');
            }else{
                $(elem).removeClass('display-on');
                $(elem).addClass('display-off');

                if(count == 0 && input.length >= 3){
                    $("#search-not").removeClass('display-off');
                    $("#search-not").addClass('display-on');
                }
            }
        });

    }, 100);

    if(input.length == 0){
        $("#search-not").removeClass('display-on display-off');
        $("#search-not").addClass('display-off');
    }   

});

/**
 * Captura evento keyup da Classe error-field
 */
 $(document).on('keyup','.error-field', function(){
    validateElement($(this));
});

/**
 * Captura evento da change da Classe error-field
 */
$(document).on('change','.error-field', function(){
    validateElement($(this));
});

/**
 * Captura evento keyup da Classe valid-field
 */
$(document).on('keyup','.valid-field', function(){
    validateElement($(this));
});

/**
 * Captura evento change da valid-field 
 */
$(document).on('change','.valid-field', function(){
    validateElement($(this));
});

/**
 * Função de confirmação se senha
 * 
 */
$(document).on('keyup', '#idsenha, #idsenha_confirmar', function (event) { 
    validateElement($(`#idsenha`));
    validateElement($(`#idsenha_confirmar`));
});

/**
 * Função de confirmação se senha
 * 
 */
$(document).on('keyup', '#idsenha, #idsenha_confirmar', function (event) { 

    if ($('#idsenha').val() == "" && $('#idsenha_confirmar').val() == "") {
        $('#confirmacao-senha').html('').css('color', '');
        return;
    }

    if ($('#idsenha').val() == $('#idsenha_confirmar').val()) {
      $('#confirmacao-senha').html('<strong>Senhas conferem</strong>').css('color', 'green');
    } else 
      $('#confirmacao-senha').html('<strong>Senhas não conferem</strong>').css('color', 'red');
});