/**
 * 
 * @param {*} options 
 */
 function executeScriptAjax(options){
    var options = $.extend(
        {
            urlAction: null,
            method: 'POST',
            data: null,
            callback: null
        },
        options    
    );

    $.ajax({
        url: options.urlAction,
        timeout: 60000,
        json: true,
        data: options.data,
        method: options.method,
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function () {
            loadingExibir();
        },
        success: function (response) {
            options.callback(response)
        },
        error: function(response){
            options.callback(response)
        }
    });
}

/**
 * Função para carregar uma pagina via solicitação ajax
 * 
 * @param {*} url                   url de destino
 * @param {*} div                   div onde o conteudo vai ser carregado
 * @param {*} modal                 defini se é para abrir o modal ou nao
 * @param {*} titulo                titulo que o modal vai receber
 */
function loadingAjax(options) {

    var options = $.extend(
        {
            urlAction: null,
            div: 'card-container',
            modal: 'false',
            data: null,
            titulo: '',
            data: null,
            method: 'GET',
        },
        options
    );

    executeScriptAjax({
        urlAction: options.urlAction,
        method: options.method,
        data: options.data,
        callback: function (response) {
            if (response.status) {
                html = response.html;
    
                //caso o modal for true o conteudo é jogado dentro do modal
                if(options.modal == 'true'){
                    modalGeneric({
                        size: 'modal-xl',
                        body: html
                    });

                    loadingScripts();
                    loadingOcultar();
                    return;
                }
    
                // //caso exista um modal aberto
                if($('.modal').is(':visible')){
                    $(".modal").modal('hide');
                }
    
                //adiciona a resposta do ajax dendo da div especificada
                $(".".concat(options.div)).html(html);
                pageUp();
    
                loadingScripts();
                loadingOcultar();
            } else {
                processModalMessage(response);
            }
        }
    });
}

function submitForm(options) {

    var options = $.extend(
        {
            idForm: null, 
            urlAction: null, 
            urlRedirect: null, 
            div: null, 
            modal: 'false', 
            title: null, 
            method: 'POST'
        },
        options
    );

    (async () => {
        //chama a funcao para validar o formulario
        var result = await validateForm(options.idForm);
        if ($('.error').length > 0 || $('.error-field').length > 0 || result.formdata === false) {
            //caso o formulario nao esteja validado é chamado a funcao para exibir os erros
            processModalMessage(result);
            return false;
        }

        executeScriptAjax({
            urlAction: options.urlAction,
            method: options.method,
            data: result.formdata,
            callback: function (response) {

                if(response.code === 666){
                    processRegisterModal(response);
                    loadingOcultar();
                    return false;
                }

                if (response.status) {
                    (async () => {
                        let y = await loadingAjax(options.urlRedirect);
                        processAlertMessage(response);
                    })();
                } else {
                    loadingOcultar();
                    processAlertMessage(response);
                }
            }

        });

    })().catch((e) => {
        loadingOcultar();
        console.log('ERRO ->', e)
    });
}

function processRegisterModal(response){
    var clients = "";
    let obj = $("#idcliente").val();
    Object.keys(obj).forEach(function(item){
        //clients += `<p>Cliente/: ${obj[item]}</p>`;
        clients += `<input type="hidden" name="idCliente[]" value="${obj[item]}">`;
    });

    var order = "";
    let obj2 = $("#idPedido").val();
    Object.keys(obj2).forEach(function(item){
        order += `<p>Cliente/Pedido: ${obj2[item]}</p>`;
        order += `<input type="hidden" name="idPedido[]" value="${obj2[item]}">`;
    });

    let html = `
        <h4>Este usúario já está cadastrado. Deseja vincular este usúario aos cliente selecionados?</h4>
        ${order}
        ${clients}
    `;

    modalConfirm({
        title: response.title,
        body: html,
        labelOk: "SIM",
        callback: function() {

            let form = new FormData($("#form-modal-corfirm")[0]);

            executeScriptAjax({
                urlAction: window.baseUrl.concat("/usuario-update-relacionamento"),
                method: "POST",
                data: form,
                callback: function (response) {
                    if (response.status) {
                        (async () => {
                            let y = await loadingAjax({
                                urlAction: window.baseUrl.concat("/usuario")
                            });
                            processAlertMessage(response);
                        })();
                    } else {
                        loadingOcultar();
                        processAlertMessage(response);
                    }
                }
            });
        }
    });
}

/**
 * Função para carregar o javascrip apos requisição 
 */
function loadingScripts() {

    var cpfMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length > 11 ? '00.000.000/0000-00' : '000.000.000-009';
    },
    cpfOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(cpfMaskBehavior.apply({}, arguments), options);
        }
    };

    $('.date').mask('00/00/0000');
    $('.anosafra').mask('0000/0000');
    $('.time').mask('00:00:00');
    $('.cpf').mask('000.000.000-00', { reverse: true });
    $('.cnpj').mask('00.000.000/0000-00', { reverse: true });
    $('.cep').mask('00000-000');
    $('.cpfCnpj').mask(cpfMaskBehavior, cpfOptions);
    $('.money2').mask("#.##0,00", { reverse: true });
    $('.money4').mask("#.##0,0000", { reverse: true });
    $('.number').mask('#0', { reverse: true });
    $('.ip_address').mask('099.099.099.099');

    $('.celular').mask("(00) 0000-00000")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(00) 00000-0000");  
            } else {  
                element.mask("(00) 0000-00000");  
            }  
        });


    //inicia o selec 2 para a classe select2 sempre que uma pagina é carregada
    $('.select2').select2({
        //theme: "bootstrap", // tema usado no select2
        width: 'resolve'
    });

    var alert = $('.alert');
    alert.each(function () {
        var that = $(this);
        setTimeout(function () {
            that.alert('close');
        }, 5000);
    });
}

/**
 * Função para exibir o loading
 */
function loadingExibir() {
    //adiciona a classe no body para o loading aparecer
    document.body.classList.add("loading");
}

/**
 * Função para ocultar o loading 
 */
function loadingOcultar() {
    //remove a classe do body 
    document.body.classList.remove("loading");
}

/**
 * Função para converter numeros decimal USA para decimal BR
 * 
 * @param {*} cur                       valor do input a ser formatado
 * @param {*} len                       tamanho de caracteres permitidos no campo
 * @param {*} decimal                   numero de casas decimais que o numero deve ter (padrão é 2)
 * @returns                             valor formatado de acordo com os parametros
 */
function decimalBr(cur, len = 20, decimal = 2) {
    n = '__0123456789';
    d = cur.value;
    l = d.length;
    r = '';
    if (typeof decimal == 'undefined') { var deci = Number(2); } else { var deci = Number(decimal); }
    var zerosprimeirodigito = '';
    for (i = 0; i < (deci - 1); i++) {
        zerosprimeirodigito = zerosprimeirodigito + '0';
    }
    if (l > 0) {
        z = d.substr(0, l - 1);
        s = '';
        a = 2;
        for (i = 0; i < l; i++) {
            c = d.charAt(i);
            if (n.indexOf(c) > a) {
                a = 1;
                s += c;
            };
        };
        l = s.length;
        t = len - 1;

        if (l > t) {
            l = t;
            s = s.substr(0, t);
        };
        if (l > deci) {
            if (deci > 0) {
                r = s.substr(0, l - deci) + ',' + s.substr(l - deci, deci);
            } else {
                r = s.substr(0, l - deci);
            }
        }
        else {
            var zerosdecimal = '';
            for (i = 0; i < (Number(deci) - Number(l)); i++) {
                zerosdecimal = zerosdecimal + '0';
            }
            if (l <= deci) {
                r = '0,' + zerosdecimal + s;
            }
        };

        if (r == '') {
            r = '0,' + zerosdecimal;
        }
        if (l > deci) {
            l = r.length;
            if (deci > 0) {
                var mensurarponto = deci + 4;
                mensurarponto = Number(mensurarponto);
                var tamanhoparaponto = mensurarponto / 2;

            } else {
                var mensurarponto = deci + 3;
                mensurarponto = Number(mensurarponto);
                var tamanhoparaponto = mensurarponto;

            }
            tamanhoparaponto = Number(tamanhoparaponto);
            if (l > mensurarponto) {
                j = l % tamanhoparaponto;
                w = r.substr(0, j);
                wa = r.substr(j, l - j - mensurarponto);
                wb = r.substr(l - mensurarponto, mensurarponto);
                if (j > 0) {
                    w += '.';
                };
                if (deci > 0) { k = (l - j) / tamanhoparaponto - 2; } else {
                    k = (l - j) / tamanhoparaponto - 1;
                }
                for (i = 0; i < k; i++) {
                    w += wa.substr(i * tamanhoparaponto, 3) + '.';
                };
                r = w + wb;
            };
        };
    };
    if (r.length <= len) {
        cur.value = r;
    }
    else {
        cur.value = z;
    };
    return 'ok';
};

/**
 * Função responsavel por subir a pagina apos solicitações ajax
 * 
 * @returns             retorna false
 */
function pageUp() {
    $('body,html').animate({
        scrollTop: 0
    }, 800);
    return false;
}

/**
 * Função para resetar os campos de um formulario
 * 
 * @param {*} element          id do form que deve ser resetado
 */
function resetForm(element) {
    //pega o forma pelo seu id
    let form = document.getElementById(element);

    //percore todos os elementos do formu crud
    for (let i = 0; i < form.elements.length; i++) {
        form.elements[i].value = '';
        form.elements[i].classList.remove('valid-field', 'error-field', 'valid', 'error');
    }

    //remove os labels criados
    $(".label-error").remove();
}

/**
 * Funcao responsavel por validar o formulario e criar um FormData para envio das informações
 * 
 * @param {*} id            id do form a ser validado
 * @returns                 retorna uma lista dos campos que deram errado e o formdata
 */
 var itemX = [];
 async function validateForm(id){
     try {
         //pera o form crud 
         let form = document.getElementById(id);
         //percore todos os elementos do formu crud
         
         var x = await processValidation(form);
 
         //variavel do formulario
         let formdata = null;
         let status = true;
         if(itemX.length != ''){
             formdata = false;
             status = false
         }else{
             formdata = new FormData(form);
         }
 
         //retorna a lista dos elementos item e o formdata criado
         return {
             "title": "",
             "status": status,
             "alertColor": "",
             "item": itemX, 
             "formdata": formdata
         }
 
     } catch(error) {
         console.log(error);
     }
 }


function processValidation(form){
    itemX = [];    
    for(i = 0; i < form.elements.length; i++){
        //identifica a tag do elemento
        let tag = form.elements[i].tagName.toLowerCase();
        //identifica o type do elemento
        let tipo = form.elements[i].type.toLowerCase();
        //validacao
        let validacao = false;
        
        //somente se a tag for textarea | input | select
        if(tag == "textarea" || tag == "input" || tag == "select"){

            //pega o value do elemento
            let valor = trim(form.elements[i].value);

            //tratamento para tipo radio  
            if(tipo === "radio"){

                //pega todos os radios do mesmo nome
                let radios = $(form).find("[name='"+form.elements[i].name+"']");
                let checked = false;
                let required = false;
                //percorre os radios capturados
                for (z = 0; z < radios.length; z++) { 
                    //se identificar que algum radio dos selecionados esta checked
                    //marca a variavel como true e pega o value e adiciona na variavel 
                    if(radios[z].checked){
                        checked = true;
                    }   
                    //verifica se algum radio esta como obrigatorio
                    if(radios[z].dataset.required == 'true'){
                        required = true;
                    }
                }

                //verifica se o radio é obrigatorio e esta marcado
                if(!(checked) && required){
                    //informa que a validacao do input nao esta valido
                    validacao = false;
                    
                    //verifica se na lista de item ja existe o elemento
                    if(!verificaValorArray(itemX, form.elements[i].name, 'nome')){
                        //array os campos que são obrigatorios
                        itemX.push(
                            {
                                "input": form.elements[i].name,
                                "message": `Campo ${form.elements[i].title} é requerido.`
                            }
                        );
                    }

                }else{
                    //funcao responsavel por aplicar classe error e label de error
                    errorForm(form.elements[i].name, true);
                }

            }else if (tipo === "checkbox") {

                //verifica se o campo é obrigatorio ou não "dataset.required" tem q ser true
                if (form.elements[i].dataset.required == 'true' && !(form.elements[i].checked)) {
                    //informa que a validacao do input nao esta valido
                    validacao = false;
                    
                    //array os campos que são obrigatorios
                    itemX.push(
                        {
                            "input": form.elements[i].name,
                            "message": `Campo ${form.elements[i].title} é requerido.`
                        }
                    );

                }else{
                    //funcao responsavel por aplicar classe error e label de error
                    errorForm(form.elements[i].name, true);
                }

            }else if (tipo === 'file'){
                //verifica se o campo é obrigatorio ou não "dataset.required" tem q ser true
                if (form.elements[i].dataset.required === 'true' && parseInt(form.elements[i].files.length) === 0) {
                    //informa que a validacao do input nao esta valido
                    validacao = false;
                    
                    //array os campos que são obrigatorios
                    itemX.push(
                        {
                            "input": form.elements[i].name,
                            "message": `Campo ${form.elements[i].title} é requerido.`
                        }
                    );

                }else{

                    //verifica se existe um arquivo selecionado
                    if(parseInt(form.elements[i].files.length) > 0){
                        let erro = 0;
                        //verificacao de extensão por padrão vem comente o PDF
                        let extPermitidas = ['pdf'];
                        if(typeof (form.elements[i].accept) !== 'undefined' && form.elements[i].accept !== ''){
                            extPermitidas = form.elements[i].accept.replace(/\./g, "").toLocaleLowerCase().split(",");
                        }

                        if(typeof extPermitidas.find(function(ext){ return valor.split('.').pop().toLocaleLowerCase() == ext; }) == 'undefined') {
                            //informa que a validacao do input nao esta valido
                            validacao = false;
                            
                            erro++;
                            //array os campos que são obrigatorios
                            itemX.push(
                                {
                                    "input": form.elements[i].name,
                                    "message": `Campo ${form.elements[i].title}. Extensão não permitida.`
                                }
                            );
                        }

                        //verificacao de tamanho padrão
                        let tamanho = 5242880; // 5 megas 5242880
                        if(typeof (form.elements[i].dataset.size) !== 'undefined' && form.elements[i].dataset.size !== ''){
                            tamanho = parseInt(form.elements[i].dataset.size);
                        }

                        if(form.elements[i].files[0].size > tamanho && erro === 0){
                            //informa que a validacao do input nao esta valido
                            validacao = false;

                            erro++;
                            //array os campos que são obrigatorios
                            itemX.push(
                                {
                                    "input": form.elements[i].name,
                                    "message": `Campo ${form.elements[i].title}. Tamanho do aquivo inválido.`
                                }
                            );
                        }
                    }
                }

            } else {
                //verifica se o campo é obrigatorio ou não "dataset.required" tem q ser true
                if (form.elements[i].dataset.required == 'true' && valor == '') {
                    //informa que a validacao do input nao esta valido
                    validacao = false;

                    //array os campos que são obrigatorios
                    itemX.push(
                        {
                            "input": form.elements[i].name,
                            "message": `Campo ${form.elements[i].title} é requerido.`
                        }
                    );

                }else{
                    //informa que a validacao do input nao esta valido
                    validacao = true;
                    //funcao responsavel por aplicar classe error e label de error
                    errorForm(form.elements[i].name, true);
                }
            } 

            //captura o data-type se existir
            let datatipo = form.elements[i].dataset.type;
            let resultado = "";
            //se o validacao é true quer dizer que passou nas validacoes anteriores
            if(validacao){
                //verifica se existe um datatipo vindo do data-type do elemento
                if(typeof datatipo !== 'undefined'){
                    //verifica o tipo de validacao que deve ser executado
                    switch(datatipo){
                        case '':
                            
                        break;
                    }

                    if(!resultado.status){
                        validacao = false;
                        itemX.push(
                            {
                                "name": form.elements[i].name,
                                "message": resultado.mensagem
                            }
                        );
                    }else{
                        //chama o erro forma passando o resuntado da validacao
                        errorForm(form.elements[i].name, resultado.status, resultado.mensagem);
                        validacao = true;
                    }
                }  
            }

            //captura o data-function se existir
            let datafunction = form.elements[i].dataset.function;
            //se o validacao é true quer dizer que passou nas validacoes anteriores
            if(validacao){
                //verifica se existe um datafunction vindo do data-function do elemento
                if(typeof datafunction !== 'undefined'){ 
                    let nome = form.elements[i].name;
                    (async () => {
                        //faz a chamadada da funcao passando o form como parametro
                        var resultado2 = await window[datafunction](form);

                        //verifica o resultado2
                        if(resultado2.status){
                            errorForm(nome, resultado2.status, resultado2.mensagem);
                        }else{
                            itemX.push(
                                {
                                    "input": nome,
                                    "message": resultado2.message
                                }
                            );
                        }

                    })().catch((e) => {console.log('ERRO ->',e)});
                }
            }
        }
    }

    return true;
}

/**
 * Função responsavel por validar um elemento somente
 * 
 * @param {*} id                id do elemento que esta recebendo o evento
 */
 function validateElement(element){
    //pera o form crud 
    let elemento = element;
    //identifica o type do elemento
    let tipo = $(elemento).attr("type");
    //pega o value do elemento
    let valor = trim($(elemento).val());
    //validacao
    let validacao = false;

    //tratamento para tipo radio e checkbox
    if (tipo == "radio"){
        
        let nome = $(elemento).attr('name');

        //pega todos os radios do mesmo nome
        let radios = $("[name='"+nome+"']");
        let checked = false;
        let required = false;
        //percorre os radios capturados
        for (z = 0; z < radios.length; z++) { 
            //se identificar que algum radio dos selecionados esta checked
            //marca a variavel como true e pega o value e adiciona na variavel 
            if(radios[z].checked){
                checked = true;
            }   
            //verifica se algum radio esta como obrigatorio
            if(radios[z].dataset.required == 'true'){
                required = true;
            }
        }

        //verifica se o radio é obrigatorio e esta marcado
        if(!(checked) && required){
            validacao = false;
            //funcao responsavel por aplicar classe error e label de error
            errorForm($(elemento).attr("name"), false);
        }else{
            validacao = true;
            errorForm($(elemento).attr("name"), true);
        }

    }else if(tipo == "checkbox") {

        //verifica se o campo é obrigatorio ou não "dataset.required" tem q ser true
        if ($(elemento).attr("data-required") === 'true' && $(elemento).is(':checked') === false) {
            validacao = false;
            //funcao responsavel por aplicar classe error e label de error
            errorForm($(elemento).attr("name"), false);
        }else{
            validacao = true;
            //funcao responsavel por aplicar classe error e label de error
            errorForm($(elemento).attr("name"), true);
        }
    }else if (tipo === 'file'){

        //verifica se o campo é obrigatorio ou não "dataset.required" tem q ser true
        if ($(elemento).attr("data-required") === 'true' && parseInt($(elemento)[0].files.length) === 0) {
            //funcao responsavel por aplicar classe error e label de error
            errorForm($(elemento).attr("name"), false);

        }else{
            //verifica se existe um arquivo selecionado
            if(parseInt($(elemento)[0].files.length) > 0){
                let erro = 0;
                //verificacao de extensão PDF por padrão
                let extPermitidas = ['pdf'];
                if(typeof ($(elemento).attr("accept")) !== 'undefined' && $(elemento).attr("accept") !== ''){
                    extPermitidas = $(elemento).attr("accept").replace(/\./g, "").toLocaleLowerCase().split(",");
                }

                if(typeof extPermitidas.find(function(ext){ return $(elemento).val().split('.').pop().toLocaleLowerCase() == ext; }) == 'undefined') {
                    validacao = false;
                    errorForm($(elemento).attr("name"), false, 'Extensão inválida');
                    erro++;
                }

                //verificacao de tamanho padrão 5 megas por padrão
                let tamanho = 5242880; // 5 megas 5242880
                if(typeof ($(elemento).attr('data-size')) !== 'undefined' && $(elemento).attr('data-size') !== ''){
                    tamanho = parseInt($(elemento).attr('data-size'));
                }

                if($(elemento)[0].files[0].size > tamanho && erro === 0){
                    validacao = false;
                    errorForm($(elemento).attr("name"), false, 'Tamanho do arquivo inválida.');
                    erro++;
                }

                if(erro === 0){
                    validacao = true;
                    errorForm($(elemento).attr("name"), true);
                }
            }
        }

    }else {
        //verifica se o campo é obrigatorio ou não "dataset.required" tem q ser true
        if ($(elemento).attr("data-required") == 'true' && valor == '') {
            validacao = false;
            //funcao responsavel por aplicar classe error e label de error
            errorForm($(elemento).attr("name"), false);

        }else{
            validacao = true;
            //funcao responsavel por aplicar classe error e label de error
            errorForm($(elemento).attr("name"), true);
        }
    }   
    
     //se o validacao é true quer dizer que passou nas validacoes anteriores
    if(validacao){
        //pega o tipo de validacao do elemento
        let datatipo = $(elemento).attr('data-type');
        let resultado = "";
        if(typeof datatipo !== 'undefined'){
            //verifica qual funcao deve ser chamada
            switch(datatipo){
                case '':
                break;
            }

            errorForm($(elemento).attr("name"), resultado.status, resultado.mensagem);
        
            if(!resultado.status){
                validacao = false;
            }
        }  
    }

    //captura o data-function se existir
    let datafunction = $(elemento).attr('data-function');
    //se o validacao é true quer dizer que passou nas validacoes anteriores
    if(validacao){
        //verifica se existe um datafunction vindo do data-function do elemento
        if(typeof datafunction !== 'undefined'){ 
            (async () => {
                //faz a chamadada da funcao passando o form como parametro
                var resultado2 = await window[datafunction]();

                errorForm($(elemento).attr("name"), resultado2.status, resultado2.message);

            })().catch((e) => {console.log('ERRO ->',e)});
        }
    }
}

/**
 * Função para adicionar/remove a classe error no fieldset
 * 
 * @param {*} nameInput             name do elemento filho do fieldset
 * @param {*} valid                 defini o tipo de validacao true ou false
 * @param {*} message               mensagem que deve ser exibida no label de err
 */
 function errorForm(nameInput, valid = false, message = "Campo Obrigatório"){
    //verifica se o id nao é vazio
    if(nameInput != ''){
        let element = null;

        //pega o elemendo pai fieldset
        element = $("[name='"+nameInput+"']");

        //verifica o tipo de valid
        if(valid){
            //se existe uma classe error é retirado a classe e
            //o display da label é setado como none
            if($(element).hasClass('error-field')){
                $("[name='"+nameInput+"']").removeClass('error-field');
                $("[name='"+nameInput+"']").addClass('valid-field');
                $("[name='"+nameInput+"-error']").remove();
            }

        }else{
            //caso ja exista a classe erro no element pai 
            //o display da label é setado como block
            if($(element).hasClass('error-field')){
                $("[name='"+nameInput+"-error']").css('display', 'block');
                $("[name='"+nameInput+"-error']").html(message);
            }else{
                //caso nao exista a classe error, é adicionado junto com a label
                $("[name='"+nameInput+"']").removeClass('valid-field');
                $("[name='"+nameInput+"']").addClass('error-field');
                $(element).after("<label for='"+nameInput+"-error' id='"+nameInput+"-error' name='"+nameInput+"-error' class='label-error'>"+message+"</label>");
            }
        }
    }
}

/*
    Função para verificar se ja existe um valor em uma determinada chave

    @param array            array a ser verificado
    @param valor            valor a ser pesquisado
    @param chave            chave que dever ser verificada o valor
*/
function verificaValorArray(array, valor, chave) {
    try {
        let tamanho = array.length;
        for (let x = 0; x < tamanho; x++) {
            if (array[x][chave] == valor) {
                return true;
            }
        }
    } catch (e) {
        console.error('erro verifica array - ', e)
    }
}

/**
 * Função para remover espaçoes em branco de um input
 * 
 * @param {*} str                   valor do input a ser limpo
 * @returns 
 */
 function trim(str) {
    if(typeof str === 'string'){
        return str.replace( /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "" );
    }

    return str;
};

/**
 * Função responsavel criar a mensagem dos campos bloqueados e chamar o modal
 * 
 * @param {*} blocked           Array dos elementos bloqueados
 * @param {*} typeView          tipo de exibicao que deve acontecer 1 para multiplos 
                                campos onde se sabe o nome do campo | 2 para mensagens
                                unicas sem repeticao.
 */
function processModalMessage(response) {

    let name = "";
    let messageSingle = "";
    let message = "";

    try {

        let total = response.item.length;

        for (let x = 0; x < total; x++) {

            if (name === "") {
                name = response.item[x]["input"];
            }

            if (name == response.item[x]["input"]) {
                messageSingle += response.item[x]["message"] + '<br/>';
            } else {
                messageSingle = response.item[x]["message"] + '<br/>';
            }

            name = response.item[x]["input"];
            message += response.item[x]["message"] + '<br/>';
            errorForm(name, response.item[x]["status"], messageSingle);    
        }

        //verifica se tem itens que devem ser blockeds
        if (total > 0) {
            loadingOcultar();
            pageUp();
            modalAlert({
                message: message,
                title: response.title,
                labelOk: "Confirmar"
            });
        }

    } catch (e) {
        loadingOcultar();
        console.log('ERRO ->', e)
    };
}

/**
 * Função para criar o alert para exibição da mensagem de erro logo acima da grid
 * 
 * @param blocked               array com os dados da mensagem de erro a ser exibida
 */
function processAlertMessage(response) {

    try {

        //percorre todas as mensagens e concatena
        let message = response.item.reduce((accumulator, { message }) => `${accumulator} ${message}<br/>`, '');

        //cria o alerta
        let alert = `<div class="alert ${response.alertColor} alert-dismissible fade show" role="alert">
                        <h5 class="alert-heading">${response.title}</h5>
                            ${message}
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>`;

        
        $('.alert-message').html(alert); 

    } catch (e) {
        loadingOcultar();
        console.log('ERRO ->', e)
    };
}

/**
 * Função para abrir o menu sidebar do item selecionado 
 */
 function openSidebar(){
    //pega o item q esta ativo
    let filho = $(".active");

    //procura o pai dele
    let pai = $(filho).closest('div .collapse');

    if(typeof pai != 'undefined'){
        //com o id do pai, busca o avo do elemento pelo data-target
        let vo = $("[data-target='#"+$(pai).prop('id')+"']")[0];

        $(vo).removeClass('collapsed').addClass('active');
        $(pai).addClass('show');
    }
}

/**
 *  Função para verificar se a senha digitada é igual a senha de confirmação
 * @param {*} form 
 * @returns 
 */
 function checkedPassword(form = null){

    let pass1 = $('#idsenha').val();
    let pass2 = $('#idsenha_confirmar').val();

    if (pass1 == "" && pass2 == "") {
        return {
            "status": true, 
            "mensagem": ""
        }
    }

    let status = false;
    let message = "";
    if (pass1 == pass2) {
        status = true;
        message = "Senhas conferem";
    } else {
        status = false;
        message = "Senhas não conferem";
    }

    return {
        "status": status, 
        "message": message
    }
}