function modalConfirm(options) {
    var options = $.extend(
        {
            body: null,
            title: null,
            urlAction: null,
            urlRedirect: null,
            labelOk: "Ok",
            labelCancel: 'Cancelar',
            danger: false,
            disabled: false,
            callback: function () { },
        },
        options
    );

    var modal = $("#modal-confirm");
    modal.find(".modal-title").html(options.title);
    modal.find(".modal-body").html(options.body);
    modal.find("form").attr('data-url', options.urlAction);
    modal.find("form").attr('data-redirect', options.urlRedirect);
    modal.find(".btn-cancel").html(options.labelCancel);
    modal.find(".btn-ok")
        .removeClass("btn-danger btn-warning")
        .addClass(options.danger ? "btn-danger" : "btn-warning")
        .off("click")
        .on("click", function () {
            modal.modal("hide");
            if (options.callback) {
                options.callback();
            }
        })
        .attr("disabled", options.disabled)
        .html(options.labelOk);
    modal.modal({
        show: true,
        backdrop: true,
        focus: true,
        keyboard: false
    });
}

function modalAlert(options){
    var options = $.extend(
        {
            message: null,
            title: null,
            labelOk: "Ok",
            type: null,
        },
        options
    );

    var modal = $("#modal-alert");
    modal.find(".modal-title").html(options.title);
    modal.find(".message").html(options.message);
    modal.find(".btn-ok").html(options.labelOk);

    modal.modal({
        show: true,
        backdrop: true,
        focus: true,
        keyboard: false
    });
}

function modalGeneric(options){
    var options = $.extend(
        {
            size: null,
            body: null
        },
        options
    );

    var modal = $("#modal-generic");
    modal.find(".modal-dialog").removeClass('modal-sm modal-lg modal-xl');
    modal.find(".modal-dialog").addClass(options.size);
    modal.find(".modal-content").html(options.body);

    modal.modal({
        show: true,
        backdrop: true,
        focus: true,
        keyboard: false
    });

    modal.modal('handleUpdate');
    loadingScripts();
}

/**
 * 
 */
 $("#form-modal-login").on("submit", function(event){
    event.preventDefault();

    let formData = new FormData($("#form-modal-login")[0]);

    executeScriptAjax({
        urlAction: window.baseUrl.concat("/login"),
        data: formData,
        method: "POST",
        callback: function(response) {
            loadingOcultar();
            switch (response.responseType) {
            case 'redirect':
                window.location.href = window.baseUrl.concat(response.path);
                break;
            case 'response':
                if (response.status) {
                    $("#modal-login").modal("hide");
                } else {
                    processAlertMessage(response);
                }
                break;
            default:
                console.error("Invalid Response Type: " + response.response_type);
            }
        }
    });

});

$("#modal-login-error-x").click(function () {
    $("#modal-login-error").hide();
});