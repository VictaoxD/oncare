$("a.toogle-recovery").click(function () {
    $(".form-login").fadeOut(100, function() {
        $(".form-recovery").fadeIn(500);
    });
    $(".g-recaptcha").fadeOut(100, function() {
        $(".g-recaptcha").fadeIn(500);
    });
    $("#inputLogin").val("");
    $("#inputPassword").val("");
    $(".alert").hide();
});

$("a.toogle-login").click(function () {
    $(".form-recovery").fadeOut(100, function() {
        $(".form-login").fadeIn(500);
    });
    $(".g-recaptcha").fadeOut(100, function() {
        $(".g-recaptcha").fadeIn(500);
    });
    $("#inputEmail").val("");
    $(".alert").hide();
});

$(".toggle-password").click(function () {
    let idPassword = $(this).closest(".input-group").find(":input");
    console.log(idPassword.attr("type"));
    if (idPassword.attr("type") == "text") {
        idPassword.attr("type", "password");
    } else {
        idPassword.attr("type", "text");
    }
});

function enableBtn() {
    $("button").prop("disabled", false);
}
