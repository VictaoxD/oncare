<?php
namespace src\api;

use Exception;
use src\helpers\FunctionHelper;

$errors = [];

class JsonAjax {

    private static function call_exception($function)
    {
        throw new Exception("JsonAjax API: Invalid '$function' format");
    }

    private static function json($array)
    {
        echo json_encode($array);
        exit;
    }

    public static function createMessage($status, $message = null, $title = '', $code = null)
    {
        if ($status) {
            $alertColor = 'alert-success';
        } else {
            $alertColor = 'alert-danger';
        }

        $ret = [
            'dateHour' => FunctionHelper::dateHourEUAtoBRA(FunctionHelper::currentTimeDate()),
            'code' => $code,
            'status' => $status,
            'title' => $title,
            'alertColor' => $alertColor,
            'item' => [
                [
                    "titleInput" => "",
                    "nameInput" => "",
                    "message" => $message,
                ]
            ]
        ];

        return $ret;
    }

    public static function response($status, $message = null, $title = '', $code = null)
    {
        if (!$status && is_null($message)) {
            self::call_exception(__FUNCTION__);
        }

        $r = self::createMessage($status, $message, $title, $code);

        $r['responseType'] = __FUNCTION__;
        self::json($r);
    }

    public static function redirect($path)
    {
        $r = [
            'path' => $path,
        ];


        $r['responseType'] = __FUNCTION__;
        self::json($r);
    }

    public static function appendError($message, $nameInput = '')
    {
        global $errors;

        $errors[] = [
            'message' => $message,
            'nameInput' => $nameInput,
        ];
    }

    public static function hasError()
    {
        global $errors;

        if (empty($errors)) {
            return false;
        } else {
            return true;
        }
    }

    public static function responseErrorIfExist($title)
    {
        global $errors;

        if (!self::hasError()) {
            return false;
        }

        $r = [
            'dateHour' => FunctionHelper::dateHourEUAtoBRA(FunctionHelper::currentTimeDate()),
            'code' => null,
            'status' => false,
            'title' => $title,
            'alertColor' => 'alert-danger',
        ];

        $r['item'] = $errors;

        $r['responseType'] = __FUNCTION__;
        self::json($r);
    }

    public static function responseHTML($HTML)
    {
        // If responseHTML, status must be true
        $r = [
            'dateHour' => FunctionHelper::dateHourEUAtoBRA(FunctionHelper::currentTimeDate()),
            'code' => null,
            'status' => true,
            'html' => $HTML,
        ];

        $r['responseType'] = __FUNCTION__;
        self::json($r);
    }
}
