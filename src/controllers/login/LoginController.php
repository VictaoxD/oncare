<?php
namespace src\controllers\login;

use \core\Controller;

class LoginController extends Controller {

    public function index() {
        $data = array();
        $data["settings"] = [
            "title" => "Seu Espaço",
        ];
        $this->renderTemplate('pages/login/login', $data);
    }

}