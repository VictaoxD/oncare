<?php
namespace src\controllers\login;

use \core\Controller;
use src\api\JsonAjax;
use \src\helpers\LoginHelper;
use src\helpers\ResetPasswordHelper;

class LoginController extends Controller {

    public function logar() 
    {
        $flash = '';
        if(!empty($_SESSION['msg_flash'])){
            $flash = $_SESSION['msg_flash'];
            $_SESSION['msg_flash'] = '';
        }
        $this->render('/pages/login/login', ['flash' => $flash]);
    }

    private function isModalLoginRequest() {
        if (isset($_POST["id"]) && ($_POST["id"] === "form-modal-login")) {
            return true;
        }

        return false;
    }

    public function acessar()
    {
        $login = filter_input(INPUT_POST, 'login');
        $senha = filter_input(INPUT_POST, 'senha');

        if ($this->isModalLoginRequest()) {

            $usuario = LoginHelper::doLoginCheckedModal($login, $senha);

            if (!isset($_SESSION["session_timeout_login"])) {
                // Unexpected error, logout user
                JsonAjax::redirect('/logout');
            }

            // 'strtolower' is to guarantee the case insentive login in case of modal login
            if (strtolower($usuario["idUsuario"]) !== strtolower($_SESSION["session_timeout_login"])) {
                JsonAjax::response(false, 'Usuario Incorreto! <br> Troca de usuário não permitida');
            }

            if (LoginHelper::loginFailure_ThresholdExceeded($login)) {
                JsonAjax::response(false, 'Tentativas de login excedidas! <br> Volte à página de login');
            }

            $token = LoginHelper::doLogin($login, $senha);
            if ($token) {
                JsonAjax::response(true);
            } else {
                if (LoginHelper::loginFailure_ThresholdExceeded($login)) {
                    JsonAjax::response(false, 'Tentativas de login excedidas! <br> Volte à página de login');
                }
                JsonAjax::response(false, 'login e/ou senha não conferem');
            }
        }

        if($login && $senha){

            if (!LoginHelper::checkedStatus($login,  $senha)) {
                $_SESSION['msg_flash'] = array('msg' => 'Este usuário está inativo. Por favor procure o seu administrador.', 'tipo' => 'alert-danger');
                $this->redirect('/login');
            }

            $token = LoginHelper::doLogin($login, $senha);
        
            if($token){
                $this->redirect('/');
            }else{
                if (LoginHelper::loginFailure_ThresholdExceeded($login)) {
                    $_SESSION['msg_flash'] = array('msg' => 'Tentativas de login excedidas!'.
                                                            '<br> Clique em \'Esqueci a senha\'',
                                                   'tipo' => 'alert-danger');
                } elseif ($number = LoginHelper::loginFailure_GetAttempt($login)) {
                    $_SESSION['msg_flash'] = array('msg' => 'Senha Incorreta (tentativa '.$number.')',
                                                   'tipo' => 'alert-danger');
                } else {
                    $_SESSION['msg_flash'] = array('msg' => 'login e/ou senha não conferem.',
                                                   'tipo' => 'alert-danger');
                }
                $this->redirect('/login');
            }

        }else{
            $login = filter_input(INPUT_POST, 'email');

            if (($login) && ResetPasswordHelper::resetPasswordPhase1($login)) {
                $this->render('pages/login/reset_email_ok');
            } else {
                $_SESSION['msg_flash'] = array('msg' => 'Digite os campos indicados.', 'tipo' => 'alert-danger');
                $this->redirect('/login');
            }
        }
    }

    public function logout(){

        $token = md5(session_id());

        if (isset($_GET["token"]) && $_GET["token"] == $token) {
            LoginHelper::doLogout();
            $this->redirect('/login');
        } else {
            $this->redirect('/');
        }
    }

}