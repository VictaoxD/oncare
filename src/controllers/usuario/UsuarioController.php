<?php

namespace src\controllers\usuario;

use core\Controller;
use core\Request;
use Exception;
use src\api\JsonAjax;
use src\helpers\DataTableHelper;
use src\helpers\ErrorHelper;
use src\helpers\FunctionHelper;
use src\helpers\LoginHelper;
use src\helpers\UserHelper;
use src\models\OrcamentoDAO;
use src\models\UsuarioDAO;
use src\models\UsuarioEntity;

class UsuarioController extends Controller
{
    private $identification = "usuario";
    private $modelUsuario;
    private $modelOrcamento;

    public function index(){
        $this->modelUsuario = new UsuarioDAO();
        $data = array();

        $data["settings"] = [
            "title" => "Usuários",
            "urlInsert" => $this->getBaseUrl()."/usuario-novo",
            "urlEdit" => $this->getBaseUrl()."/usuario-editar",
            "urlDelete" => $this->getBaseUrl()."/usuario-apagar",
            "urlDetail" => $this->getBaseUrl()."/usuario-detalhe",
            "urlRedirect" => $this->getBaseUrl()."/usuario"
        ];

        $data["datatable"] = DataTableHelper::getHeader(
            $this->identification, 
            0
        );

        $this->renderTemplate('pages/usuario/grid', $data);
    }

    public function pesquisa(){
        $data = array();

        $data["settings"] = [
            "title" => "Usuários",
            "urlInsert" => $this->getBaseUrl()."/usuario-novo",
            "urlEdit" => $this->getBaseUrl()."/usuario-editar",
            "urlDelete" => $this->getBaseUrl()."/usuario-apagar",
            "urlDetail" => $this->getBaseUrl()."/usuario-detalhe",
            "urlRedirect" => $this->getBaseUrl()."/usuario"
        ];

        $data["datatable"] = DataTableHelper::getHeader(
            $this->identification, 
            0
        );

        $this->renderTemplate('pages/usuario/datatable', $data);
    }

    public function novo(){

        if (Request::isPostRequest()) {
            try{
                $this->modelUsuario = new UsuarioDAO();
                $this->modelUsuario->startTransaction();


                if($this->modelUsuario->checkedRegister(new UsuarioEntity($_POST))){
                    $this->modelUsuario->rollBack();
                    JsonAjax::response(false, "Este usuario já esta cadastrado", 'Atenção!', 666);
                    return false;
                }


                $this->modelUsuario->insert(new UsuarioEntity($_POST));
                $id_insert = $this->modelUsuario->lastId(); 

                if($id_insert > 0 && isset($_POST["idCliente"])){
                    foreach($_POST["idCliente"] as $value){
                        $this->modelUsuario->insertUsuarioCliente(new UsuarioEntity(
                            [
                                "idUsuario" => $id_insert,
                                "idCliente" => $value
                            ]    
                        ));
                    }
                }

                if($id_insert > 0 && isset($_POST["idPedido"])){
                    foreach($_POST["idPedido"] as $value){
                        $x = explode('-', $value);

                        $this->modelUsuario->insertUsuarioClientePedido(new UsuarioEntity(
                            [
                                "idUsuario" => $id_insert,
                                "idCliente" => $x[0],
                                "idPedido" => $x[1]
                            ]    
                        ));
                    }
                }

                if ($this->transactionsWithoutErrors($this->modelUsuario)) {
                    $this->modelUsuario->commit();
                    JsonAjax::response(true, "Registro salvo com sucesso", 'Operação concluida com sucesso!');
                }
                exit;

            }catch(Exception $e){
                $this->modelUsuario->rollBack();
                JsonAjax::response(false, $e, "Erro processo de inserir usuário");
            }
        }

        $data = array();
        $data["settings"] = [
            "title" => "Novo Usuário",
            "view" => "usuario/novo",
            "urlAction" => $this->getBaseUrl()."/usuario-novo",
            "urlRedirect" => $this->getBaseUrl()."/usuario",
        ];
        $data['clientes'] = UserHelper::getClients();
        $this->renderTemplate('partials/crud/crud', $data);
    }

    public function editar(){
        $this->modelUsuario = new UsuarioDAO();
        $this->modelOrcamento = new OrcamentoDAO();

        $idKey = filter_input(INPUT_GET, 'idKey', FILTER_SANITIZE_SPECIAL_CHARS);
        if (Request::isPostRequest()) {
            try{
                $this->modelUsuario->startTransaction();

                $this->modelUsuario->update(new UsuarioEntity($_POST));
                $clienteUsuario = $this->modelUsuario->selectClienteUsuario($idKey);

                if(isset($_POST["idCliente"])){
                    foreach($clienteUsuario as $value){
                        if(!in_array($value, $_POST["idCliente"])){
                            $this->modelUsuario->deleteClienteUsuario($value, $idKey);
                        }
                    }

                    foreach($_POST["idCliente"] as $value){
                        if(!in_array($value, $clienteUsuario)){
                            $this->modelUsuario->insertUsuarioCliente(new UsuarioEntity(
                                [
                                    "idUsuario" => $idKey,
                                    "idCliente" => $value
                                ]    
                            ));
                        }
                    }
                }

                if(isset($_POST["idPedido"])){
                    foreach($_POST["idPedido"] as $value){
                        $x = explode('-', $value);

                        $pedidoUsuario = $this->modelUsuario->selectPedidoClienteUsuario($idKey, $x[0]);

                        foreach($pedidoUsuario as $v){
                            if(!in_array($v, $_POST["idPedido"])){
                                $this->modelUsuario->deletePedidoUsuario($v, $idKey);
                            }
                        }

                        $this->modelUsuario->insertUsuarioClientePedido(new UsuarioEntity(
                            [
                                "idUsuario" => $idKey,
                                "idCliente" => $x[0],
                                "idPedido" => $x[1]
                            ]    
                        ));
                    }
                    
                }

                if ($this->transactionsWithoutErrors($this->modelUsuario)) {
                    $this->modelUsuario->commit();
                    JsonAjax::response(true, sprintf("Registro id: %s", $idKey), 'Registro editado com sucesso!');
                }
                exit;

            }catch(Exception $e){
                $this->modelUsuario->rollBack();
                JsonAjax::response(false, $e, "Erro processo de editar usuário");
            }
        }

        $data = array();
        $data["settings"] = [
            "title" => "Editar id: ".$idKey,
            "view" => "usuario/editar",
            "urlAction" => $this->getBaseUrl()."/usuario-editar?idKey=".$idKey,
            "urlRedirect" => $this->getBaseUrl()."/usuario",
        ];

        $data["usuario"] = $this->modelUsuario->select($idKey);
        $data["clienteUsuario"] = $this->modelUsuario->selectClienteUsuario($idKey);
        $data["pedidoUsuario"] = $this->modelUsuario->selectPedidoUsuario($idKey);
        $data['clientes'] = UserHelper::getClients();
        $data['pedidos'] = $this->modelOrcamento->selectPedidoCliente($data["clienteUsuario"]);
        $this->renderTemplate('partials/crud/crud', $data);
    }

    public function updateRelacionamento(){

        if (Request::isPostRequest()) {
            try{
                $this->modelUsuario = new UsuarioDAO();
                $this->modelUsuario->startTransaction();

                foreach($_POST["idCliente"] as $value){
                    $this->modelUsuario->insertUsuarioCliente(new UsuarioEntity(
                        [
                            "idUsuario" => $_SESSION["IDUSER"],
                            "idCliente" => $value,
                        ]   
                    ));
                }

                
                foreach($_POST["idPedido"] as $value){
                    $x = explode('-', $value);

                    $this->modelUsuario->insertUsuarioClientePedido(new UsuarioEntity(
                        [
                            "idUsuario" => $_SESSION["IDUSER"],
                            "idCliente" => $x[0],
                            "idPedido" => $x[1]
                        ]    
                    ));
                }
                
                if ($this->transactionsWithoutErrors($this->modelUsuario)) {
                    $this->modelUsuario->commit();
                    JsonAjax::response(true, "O usúario foi vinculado com sucesso!", 'Operação concluida com sucesso!');
                }
                exit;

            }catch(Exception $e){
                $this->modelUsuario->rollBack();
                JsonAjax::response(false, $e, "Erro processo de editar usuário");
            }
        }
    }

    public function apagar(){
        if (Request::isPostRequest()) {
            try{
                $this->modelUsuario = new UsuarioDAO();
                $this->modelUsuario->startTransaction();

                $idKey = filter_input(INPUT_POST, 'idKey', FILTER_SANITIZE_SPECIAL_CHARS);

                $this->modelUsuario->delete(new UsuarioEntity(['idUsuario' => $idKey]));

                if ($this->transactionsWithoutErrors($this->modelUsuario)) {
                    $this->modelUsuario->commit();
                    JsonAjax::response(true, sprintf("Registro id: %s excluido com sucesso", $idKey), 'Operação concluida com sucesso!');
                }
                exit;

            }catch(Exception $e){
                $this->modelUsuario->rollBack();
                JsonAjax::response(false, $e, "Erro processo de editar usuário");
            }
        }
    }

    public function detalhe(){
        $idKey = filter_input(INPUT_GET, 'idKey', FILTER_SANITIZE_SPECIAL_CHARS);

        $data = array();
        $data["settings"] = [
            "title" => "Detalhes id: ".$idKey,
            "view" => "usuario/detalhe",
            "urlRedirect" => $this->getBaseUrl()."/usuario",
        ];
        
        $this->renderTemplate('partials/modal_content/detail', $data);
    }

    public function meusDados(){
        $this->modelUsuario = new UsuarioDAO();

        $data = array();
        $data["settings"] = [
            "title" => "Meus Dados",
            "view" => "usuario/meus_dados",
            "urlRedirect" => $this->getBaseUrl()."/",
        ];

        $data["dados"] = UserHelper::getMeusDados();
        $data["usuario"] = $this->modelUsuario->select(LoginHelper::getInfoUser()->getIdUsuario());
        $this->renderTemplate('pages/usuario/meus_dados', $data);
    }

    public function editarSenha(){
        $idKey = filter_input(INPUT_GET, 'idKey', FILTER_SANITIZE_SPECIAL_CHARS);

        if (Request::isPostRequest()) {
            try{
                $this->modelUsuario = new UsuarioDAO();
                $this->modelUsuario->startTransaction();

                $senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_SPECIAL_CHARS);
                $senhaConfirme = filter_input(INPUT_POST, 'senhaConfirme', FILTER_SANITIZE_SPECIAL_CHARS);

                if($senha != $senhaConfirme){
                    $this->modelUsuario->rollBack();
                    JsonAjax::response(false, "Senhas não conferem", "Atenção!");
                }

                $this->modelUsuario->updatePassword(LoginHelper::getInfoUser()->getIdUsuario(), $senha);

                if ($this->transactionsWithoutErrors($this->modelUsuario)) {
                    $this->modelUsuario->commit();
                    JsonAjax::response(true, sprintf("Senha alterada com sucesso com sucesso", $idKey), 'Operação concluida com sucesso!');
                }
                exit;

            }catch(Exception $e){
                $this->modelUsuario->rollBack();
                JsonAjax::response(false, $e, "Erro processo de editar usuário");
            }
        }

        $data = array();
        $data["settings"] = [
            "title" => "Editar Senha",
            "view" => "usuario/editar_senha",
            "urlRedirect" => $this->getBaseUrl()."/",
        ];

        $this->renderTemplate('partials/crud/crud', $data);
    }

    public function dadosCliente(){
        $this->modelUsuario = new UsuarioDAO();

        $data = array();
        $data["settings"] = [
            "title" => "Dados Cliente",
            "view" => "usuario/dados_cliente",
            "urlRedirect" => $this->getBaseUrl()."/",
        ];

        $data["dados"] = UserHelper::getDadosCliente();
        $data["usuario"] = $this->modelUsuario->select(LoginHelper::getInfoUser()->getIdUsuario());
        $this->renderTemplate('pages/usuario/dados_cliente', $data);
    }
}