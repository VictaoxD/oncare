<?php
namespace src\controllers\login_psicologo;

use \core\Controller;

class LoginPsicoController extends Controller {

    public function index() {
        $data = array();
        $data["settings"] = [
            "title" => "Psicólogos",
        ];
        $this->renderTemplate('pages/login_psicologo/login_psicologo', $data);
    }

}