<?php
namespace src\controllers\error;

use \core\Controller;

class ErrorController extends Controller {

    public function index() 
    {
        $this->renderTemplate('404');
    }

    public function permissaoNegada(){
        $this->renderTemplate('acesso_negado');
    }

}