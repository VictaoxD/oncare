<?php
namespace src\controllers\parceiros;

use \core\Controller;

class EscolasController extends Controller {

    public function index() {
        $data = array();
        $data["settings"] = [
            "title" => "Educacional",
        ];
        $this->renderTemplate('pages/parceiros/escolas', $data);
    }

}