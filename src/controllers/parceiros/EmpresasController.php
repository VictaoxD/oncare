<?php
namespace src\controllers\parceiros;

use \core\Controller;

class EmpresasController extends Controller {

    public function index() {
        $data = array();
        $data["settings"] = [
            "title" => "Corporativo",
        ];
        $this->renderTemplate('pages/parceiros/empresas', $data);
    }

}