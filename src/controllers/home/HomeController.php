<?php
namespace src\controllers\home;

use \core\Controller;

class HomeController extends Controller {

    public function index() {
        $data = array();
        $data["settings"] = [
            "title" => "Home",
        ];
        $this->renderTemplate('pages/home/home', $data);
    }

}