<?php 

namespace src\models;

use core\Entity;

class UsuarioEntity extends Entity{

    /**
     * Estrutura = ['key' => ['keyRule' => 'Rule message']]
     */
    const validations = [
        'idCliente' => ['required' => 'Campo Cliente é obrigatório'],
        'nome' => ['required' => 'Campo nome é obrigatório'],
        'email' => [
            'required' => 'Campo email é obrigatório',
            'email' => 'E-mail inválido'
        ],
        'cpfCnpj' => ['required' => 'Campo Cpf/Cnpj é obrigatório'],
        'status' => ['required' => 'Campo Status é obrigatório'],
        'senha' => ['required' => 'Campo Senha é obrigatório']
    ];

    /**
     * Estrutura = ['key' => 'rule']
     */
    const sanitize = [];

    private $idUsuario;
    private $idCliente;
    private $idPedido;
    private $idConvenio;
    private $idResponsavel;
    private $nome;
    private $cpfCnpj;
    private $email;
    private $celular;
    private $senha;
    private $token;
    private $status;
    private $tipoUsuario;
    private $ultimoLogin;
    private $clientes;
    private $convenios;
    private $idUsuarioCadastro;
    private $idUsuarioAlteracao;
    private $dataHoraAlteracao;
    private $dataHoraCadastro;

    /**
     * Get the value of idUsuario
     */ 
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set the value of idUsuario
     *
     * @return  self
     */ 
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get the value of nome
     */ 
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set the value of nome
     *
     * @return  self
     */ 
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get the value of cpfCnpj
     */ 
    public function getCpfCnpj()
    {
        return $this->cpfCnpj;
    }

    /**
     * Set the value of cpfCnpj
     *
     * @return  self
     */ 
    public function setCpfCnpj($cpfCnpj)
    {
        $this->cpfCnpj = $cpfCnpj;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of senha
     */ 
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * Set the value of senha
     *
     * @return  self
     */ 
    public function setSenha($senha)
    {
        $this->senha = $senha;

        return $this;
    }

    /**
     * Get the value of token
     */ 
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set the value of token
     *
     * @return  self
     */ 
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of ultimoLogin
     */ 
    public function getUltimoLogin()
    {
        return $this->ultimoLogin;
    }

    /**
     * Set the value of ultimoLogin
     *
     * @return  self
     */ 
    public function setUltimoLogin($ultimoLogin)
    {
        $this->ultimoLogin = $ultimoLogin;

        return $this;
    }

    /**
     * Get the value of idUsuarioCadastro
     */ 
    public function getIdUsuarioCadastro()
    {
        return $this->idUsuarioCadastro;
    }

    /**
     * Set the value of idUsuarioCadastro
     *
     * @return  self
     */ 
    public function setIdUsuarioCadastro($idUsuarioCadastro)
    {
        $this->idUsuarioCadastro = $idUsuarioCadastro;

        return $this;
    }

    /**
     * Get the value of idUsuarioAlteracao
     */ 
    public function getIdUsuarioAlteracao()
    {
        return $this->idUsuarioAlteracao;
    }

    /**
     * Set the value of idUsuarioAlteracao
     *
     * @return  self
     */ 
    public function setIdUsuarioAlteracao($idUsuarioAlteracao)
    {
        $this->idUsuarioAlteracao = $idUsuarioAlteracao;

        return $this;
    }

    /**
     * Get the value of dataHoraAlteracao
     */ 
    public function getDataHoraAlteracao()
    {
        return $this->dataHoraAlteracao;
    }

    /**
     * Set the value of dataHoraAlteracao
     *
     * @return  self
     */ 
    public function setDataHoraAlteracao($dataHoraAlteracao)
    {
        $this->dataHoraAlteracao = $dataHoraAlteracao;

        return $this;
    }

    /**
     * Get the value of dataHoraCadastro
     */ 
    public function getDataHoraCadastro()
    {
        return $this->dataHoraCadastro;
    }

    /**
     * Set the value of dataHoraCadastro
     *
     * @return  self
     */ 
    public function setDataHoraCadastro($dataHoraCadastro)
    {
        $this->dataHoraCadastro = $dataHoraCadastro;

        return $this;
    }

    /**
     * Get the value of tipoUsuario
     */ 
    public function getTipoUsuario()
    {
        return $this->tipoUsuario;
    }

    /**
     * Set the value of tipoUsuario
     *
     * @return  self
     */ 
    public function setTipoUsuario($tipoUsuario)
    {
        $this->tipoUsuario = $tipoUsuario;

        return $this;
    }

    /**
     * Get the value of clientes
     */ 
    public function getClientes()
    {
        return $this->clientes;
    }

    /**
     * Set the value of clientes
     *
     * @return  self
     */ 
    public function setClientes($clientes)
    {
        $this->clientes = $clientes;

        return $this;
    }

    /**
     * Get the value of convenio
     */ 
    public function getConvenio()
    {
        return $this->convenio;
    }

    /**
     * Set the value of convenio
     *
     * @return  self
     */ 
    public function setConvenio($convenio)
    {
        $this->convenio = $convenio;

        return $this;
    }

    /**
     * Get the value of idCliente
     */ 
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set the value of idCliente
     *
     * @return  self
     */ 
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get the value of idConvenio
     */ 
    public function getIdConvenio()
    {
        return $this->idConvenio;
    }

    /**
     * Set the value of idConvenio
     *
     * @return  self
     */ 
    public function setIdConvenio($idConvenio)
    {
        $this->idConvenio = $idConvenio;

        return $this;
    }

    /**
     * Get the value of convenios
     */ 
    public function getConvenios()
    {
        return $this->convenios;
    }

    /**
     * Set the value of convenios
     *
     * @return  self
     */ 
    public function setConvenios($convenios)
    {
        $this->convenios = $convenios;

        return $this;
    }

    /**
     * Get the value of idResponsavel
     */ 
    public function getIdResponsavel()
    {
        return $this->idResponsavel;
    }

    /**
     * Set the value of idResponsavel
     *
     * @return  self
     */ 
    public function setIdResponsavel($idResponsavel)
    {
        $this->idResponsavel = $idResponsavel;

        return $this;
    }

    /**
     * Get the value of celular
     */ 
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set the value of celular
     *
     * @return  self
     */ 
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get the value of idPedido
     */ 
    public function getIdPedido()
    {
        return $this->idPedido;
    }

    /**
     * Set the value of idPedido
     *
     * @return  self
     */ 
    public function setIdPedido($idPedido)
    {
        $this->idPedido = $idPedido;

        return $this;
    }
}