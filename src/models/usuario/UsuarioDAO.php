<?php

namespace src\models;

use \core\Model;
use Exception;
use src\helpers\FunctionHelper;
use src\helpers\LoginHelper;

class UsuarioDAO extends Model {

    public function updateToken($token, $idUsuario){
        $sql = "UPDATE usuario SET 
                    token = :token,
                    ultimoLogin = :ultimoLogin
                WHERE idUsuario = :idUsuario";

        $params = array(
            ":token" => $token,
            ":idUsuario" => $idUsuario,
            ":ultimoLogin" => FunctionHelper::currentTimeDate()
        );

        return $this->updateModel($sql, $params);
    }

    public function updatePassword($uid, $senha){
        $sql = "UPDATE usuario 
                SET senha = :senha
                WHERE idUsuario = :idUsuario";

        $params = array(
            ":senha" => md5($senha),
            ":idUsuario" => $uid
        );

        return $this->updateModel($sql, $params);
    }

    public function selectToken($token){
        $sql = "SELECT *
                FROM usuario
                WHERE token = :token
                AND usuario.status = '1'";

        $params = array(
            ":token" => $token,
        );

        return $this->selectModel($sql, $params);
    }

    public function selectEmail($email){
        $sql = "SELECT *
                FROM usuario
                WHERE email = :email 
                AND usuario.status = '1'";

        $params = array(
            ":email" => $email,
        );

        return $this->selectModel($sql, $params);
    }

    public function selectCpfCnpj($cpfCnpj){
        $sql = "SELECT *
                FROM usuario 
                WHERE cpfCnpj = :cpfCnpj";

        $params = array(
            ":cpfCnpj" => FunctionHelper::onlyNumber($cpfCnpj)
        );

        return $this->selectModel($sql, $params);
    }

    public function selectId($uid){
        $sql = "SELECT usuario.*, 
                    GROUP_CONCAT(cliente_usuario.idCliente) AS clientes,
                    GROUP_CONCAT(cliente_usuario.idConvenio) AS convenios
                FROM usuario
                LEFT JOIN cliente_usuario ON cliente_usuario.idUsuario = usuario.idUsuario
                WHERE usuario.idUsuario = :idUsuario";

        $params = array(
            ":idUsuario" => $uid,
        );

        $result = $this->selectModel($sql, $params);

        if(count($result) > 0){
            return [
                "idUsuario" => $result["idUsuario"],
                "idCliente" => $result["idCliente"],
                "idConvenio" => $result["idConvenio"],
                "nome" => $result["nome"],
                "cpfCnpj" => $result["cpfCnpj"],
                "email" => $result["email"],
                "status" => $result["status"],
                "tipoUsuario" => $result["tipoUsuario"],
                "ultimoLogin" => $result["ultimoLogin"],
                "clientes" => $result["clientes"],
                "convenios" => $result["convenios"]
            ];
        }

        return [];
    }

    public function checkedRegister(UsuarioEntity $obj){
        $sql = "SELECT usuario.idUsuario 
                FROM usuario
                WHERE usuario.cpfCnpj = :cpfCnpj";

        $params = [
            ":cpfCnpj" => FunctionHelper::onlyNumber($obj->getCpfCnpj()),
        ];

        $result = $this->selectModel($sql, $params);

        if(!empty($result["idUsuario"])){
            $_SESSION["IDUSER"] = null;
            $_SESSION["IDUSER"] = $result["idUsuario"];
            return true;
        }

        return false;
    }

    public function checkedStatusEmail($email)
    {
        $sql = "SELECT usuario.idUsuario
                FROM usuario 
                WHERE usuario.email = :email 
                AND usuario.status = '1'";

        $params = array(
            ":email" => $email
        );

        $result = $this->selectModel($sql, $params);

        if (!empty($result)) {
            return true;
        }

        return false;
    }

    public function checkedStatusCpfCnpj($cpfCnpj)
    {
        $sql = "SELECT usuario.idUsuario
                FROM usuario 
                WHERE usuario.cpfCnpj = :cpfCnpj 
                AND usuario.status = '1'";

        $params = array(
            ":cpfCnpj" => FunctionHelper::onlyNumber($cpfCnpj)
        );

        $result = $this->selectModel($sql, $params);

        if (!empty($result)) {
            return true;
        }

        return false;
    }

    public function insert(UsuarioEntity $obj){

        $user = LoginHelper::getInfoUser();

        try{
            $sql = "INSERT INTO usuario (
                nome,
                cpfCnpj,
                email,
                celular,
                senha,
                status,
                tipoUsuario,
                idUsuarioCadastro,
                idUsuarioAlteracao,
                dataHoraAlteracao,
                dataHoraCadastro
            ) VALUES (
                :nome,
                :cpfCnpj,
                :email,
                :celular,
                :senha,
                :status,
                :tipoUsuario,
                :idUsuarioCadastro,
                :idUsuarioAlteracao,
                :dataHoraAlteracao,
                :dataHoraCadastro
            )";

            $params = [
                ":nome" => FunctionHelper::upperCase($obj->getNome()),
                ":cpfCnpj" => FunctionHelper::onlyNumber($obj->getCpfCnpj()),
                ":email" => $obj->getEmail(),
                ":celular" => FunctionHelper::onlyNumber($obj->getCelular()),
                ":senha" => md5($obj->getSenha()),
                ":status" => $obj->getStatus(),
                ":tipoUsuario" => "USER",
                ":idUsuarioCadastro" => $user->getIdUsuario(),
                ":idUsuarioAlteracao" => $user->getIdUsuario(),
                ":dataHoraAlteracao" => FunctionHelper::currentTimeDate(),
                ":dataHoraCadastro" => FunctionHelper::currentTimeDate(),
            ];

            return $this->insertModel($sql, $params, $obj);

        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function insertUsuarioCliente(UsuarioEntity $obj){
        try{

            $user = LoginHelper::getInfoUser();
            $idResponsavel = 0;
            if(!empty($user->getClientes())){
                $idResponsavel = $user->getClientes();
            }else{
                $idResponsavel = $user->getConvenios();
            }
    
            $sql = "INSERT INTO cliente_usuario (
                idUsuario,
                idCliente,
                idResponsavel
            ) VALUES (
                :idUsuario,
                :idCliente,
                :idResponsavel
            )";

            $params = [
                ":idUsuario" => $obj->getIdUsuario(),
                ":idCliente" => $obj->getIdcliente(),
                ":idResponsavel" => $idResponsavel
            ];

            return $this->insertModel($sql, $params, $obj);
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function insertUsuarioClientePedido(UsuarioEntity $obj)
    {
        try{
    
            $sql = "INSERT INTO cliente_usuario_pedido (
                idUsuario,
                idCliente,
                idPedido
            ) VALUES (
                :idUsuario,
                :idCliente,
                :idPedido
            )";

            $params = [
                ":idUsuario" => $obj->getIdUsuario(),
                ":idCliente" => $obj->getIdcliente(),
                ":idPedido" => $obj->getIdPedido()
            ];

            return $this->insertModel($sql, $params, $obj);
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function update(UsuarioEntity $obj){
        try{

            if(!empty($obj->getSenha())){
                $this->updatePassUser($obj);
            }

            $sql = "UPDATE usuario SET
                        nome = :nome,
                        cpfCnpj = :cpfCnpj,
                        email = :email,
                        celular = :celular,
                        status = :status,
                        idUsuarioAlteracao = :idUsuarioAlteracao,
                        dataHoraAlteracao = :dataHoraAlteracao
                    WHERE idUsuario = :idUsuario";

            $params = [
                ":idUsuario" => $obj->getIdUsuario(),
                ":nome" => FunctionHelper::upperCase($obj->getNome()),
                ":cpfCnpj" => FunctionHelper::onlyNumber($obj->getCpfCnpj()),
                ":email" => $obj->getEmail(),
                ":celular" => FunctionHelper::onlyNumber($obj->getCelular()),
                ":status" => $obj->getStatus(),
                ":idUsuarioAlteracao" => LoginHelper::getInfoUser()->getIdUsuario(),
                ":dataHoraAlteracao" => FunctionHelper::currentTimeDate(),
            ];

            return $this->updateModel($sql, $params, $obj);

        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function updatePassUser(UsuarioEntity $obj){
        $sql = "UPDATE usuario SET
                    senha = :senha
                WHERE idUsuario = :idUsuario";

        $params = [
            ":idUsuario" => $obj->getIdUsuario(),
            ":senha" => md5($obj->getSenha())
        ];

        return $this->updateModel($sql, $params, $obj);
    }

    public function updateDados(UsuarioEntity $obj)
    {
        $sql = "UPDATE usuario SET
                    nome = :nome,
                    cpfCnpj = :cpfCnpj,
                    email = :email
                WHERE usuario.idUsuario = :idUsuario";

        $params = [
            ":idUsuario" => LoginHelper::getInfoUser()->getIdUsuario(),
            ":nome" => $obj->getNome(),
            ":email" => $obj->getEmail(),
            ":cpfCnpj" => $obj->getCpfCnpj()
        ];

        return $this->updateModel($sql, $params, $obj);
    }

    public function delete(UsuarioEntity $obj){
        try{

            $user = LoginHelper::getInfoUser();
            $idResponsavel = 0;
            if(!empty($user->getClientes())){
                $idResponsavel = $user->getClientes();
            }else{
                $idResponsavel = $user->getConvenios();
            }

            $sql = "DELETE 
                    FROM cliente_usuario
                    WHERE cliente_usuario.idUsuario = :idUsuario
                    AND cliente_usuario.idResponsavel = :idResponsavel ";

            $params = [
                ":idUsuario" => $obj->getIdUsuario(),
                ":idResponsavel" => $idResponsavel
            ];

            return $this->deleteModel($sql, $params);

        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function deleteClienteUsuario($idCliente, $idUsuario){
        try{

            $user = LoginHelper::getInfoUser();
            $idResponsavel = 0;
            if(!empty($user->getClientes())){
                $idResponsavel = $user->getClientes();
            }else{
                $idResponsavel = $user->getConvenios();
            }

            $sql = "DELETE 
                    FROM cliente_usuario
                    WHERE cliente_usuario.idUsuario = :idUsuario
                    AND cliente_usuario.idCliente = :idCliente 
                    AND cliente_usuario.idResponsavel = :idResponsavel ";

            $params = [
                ":idUsuario" => $idUsuario,
                ":idCliente" => $idCliente,
                ":idResponsavel" => $idResponsavel
            ];

            return $this->deleteModel($sql, $params);
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function deletePedidoUsuario($idPedido, $idUsuario){
        try{

            $sql = "DELETE 
                    FROM cliente_usuario_pedido
                    WHERE cliente_usuario_pedido.idUsuario = :idUsuario
                    AND cliente_usuario_pedido.idPedido = :idPedido ";

            $params = [
                ":idUsuario" => $idUsuario,
                ":idPedido" => $idPedido,
            ];

            return $this->deleteModel($sql, $params);
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function select($idKey){
        try{
            $sql = "SELECT usuario.*
                    FROM usuario
                    WHERE usuario.idUsuario = :idUsuario";

            $params = [
                ":idUsuario" => $idKey
            ];

            $result = $this->selectModel($sql, $params);

            $array = [];
            if(count($result)){
                $array = [
                    'idUsuario' => $result['idUsuario'],
                    'nome' => $result['nome'],
                    'cpfCnpj' => $result['cpfCnpj'],
                    'email' => $result['email'],
                    'status' => $result['status'],
                    'celular' => $result['celular']
                ];
            }

            return $array;

        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function selectClienteUsuario($idKey){

        $user = LoginHelper::getInfoUser();
        $idResponsavel = 0;
        if(!empty($user->getClientes())){
            $idResponsavel = $user->getClientes();
        }else{
            $idResponsavel = $user->getConvenios();
        }

        $sql = "SELECT cliente_usuario.idCliente
                FROM cliente_usuario 
                WHERE cliente_usuario.idUsuario = :idUsuario 
                AND cliente_usuario.idResponsavel = :idResponsavel";

        $params = [
            ":idUsuario" => $idKey,
            ":idResponsavel" => $idResponsavel
        ];

        $result =  $this->selectAllModel($sql, $params);
        $array = [];
        foreach($result as $value){
            $array[] = $value["idCliente"];
        }

        return $array;
    }

    public function selectPedidoClienteUsuario($idKey, $idCliente){

        $sql = "SELECT cliente_usuario_pedido.idPedido
                FROM cliente_usuario_pedido 
                WHERE cliente_usuario_pedido.idUsuario = :idUsuario 
                AND cliente_usuario_pedido.idCliente = :idCliente";

        $params = [
            ":idUsuario" => $idKey,
            ":idCliente" => $idCliente
        ];

        $result =  $this->selectAllModel($sql, $params);
        $array = [];
        foreach($result as $value){
            $array[] = $value["idPedido"];
        }

        return $array;
    }

    public function selectPedidoUsuario($idKey){

        $sql = "SELECT cliente_usuario_pedido.idPedido
                FROM cliente_usuario_pedido 
                WHERE cliente_usuario_pedido.idUsuario = :idUsuario";

        $params = [
            ":idUsuario" => $idKey
        ];

        $result =  $this->selectAllModel($sql, $params);
        $array = [];
        foreach($result as $value){
            $array[] = $value["idPedido"];
        }

        return $array;
    }

    public function selectAll(){
        try{

            $sql = "";

            $params = [];

            $result = $this->selectAllModel($sql, $params);

        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function selectGrid($sql, $order, $id){
        try{

            $user = LoginHelper::getInfoUser();
            $idResponsavel = 0;
            if(!empty($user->getClientes())){
                $idResponsavel = $user->getClientes();
            }else{
                $idResponsavel = $user->getConvenios();
            }

            $sql = "SELECT $sql
                    FROM usuario 
                    INNER JOIN cliente_usuario ON cliente_usuario.idUsuario = usuario.idUsuario
                    WHERE cliente_usuario.idResponsavel = :idResponsavel 
                    GROUP BY usuario.idUsuario
                    ORDER BY $order";

            $params = [
                ":idResponsavel" => $idResponsavel
            ];

            $result = $this->selectAllModel($sql, $params);

            $array = [];
            foreach($result as $value){
                $array[] = [
                    0 => $value['idUsuario'],
                    1 => $value['nome'],
                    2 => $value['cpfCnpj']
                ];
            }

            return $array;

        }catch(Exception $e){
            echo $e->getMessage();
        }
    }
}