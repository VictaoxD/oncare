<?php
use core\Router;
use core\Security as S;

$router = new Router();

$router->get('/', 'home@HomeController@index', [S::SEMPRE_LIBERAR]);
$router->get('/login', 'login@LoginController@index', [S::SEMPRE_LIBERAR]);
$router->post('/login', 'login@LoginController@index', [S::SEMPRE_LIBERAR]);
$router->get('/login_psicologo', 'login_psicologo@LoginPsicoController@index', [S::SEMPRE_LIBERAR]);
$router->post('/login_psicologo', 'login_psicologo@LoginPsicoController@index', [S::SEMPRE_LIBERAR]);
$router->get('/logout', 'login@LoginController@logout', [S::LOGADO_LIBERAR]);
$router->get('/login/reset', 'login@LoginController@reset_get', [S::SEMPRE_LIBERAR]);
$router->post('/login/reset', 'login@LoginController@reset_post', [S::SEMPRE_LIBERAR]);

$router->get('/parceiros/empresas', 'parceiros@EmpresasController@index', [S::SEMPRE_LIBERAR]);
$router->get('/parceiros/escolas', 'parceiros@EscolasController@index', [S::SEMPRE_LIBERAR]);

$router->get('/usuario', 'usuario@UsuarioController@index');
$router->get('/usuario-pesquisa', 'usuario@UsuarioController@pesquisa');
$router->get('/usuario-novo', 'usuario@UsuarioController@novo');
$router->post('/usuario-novo', 'usuario@UsuarioController@novo');
$router->get('/usuario-editar', 'usuario@UsuarioController@editar');
$router->post('/usuario-editar', 'usuario@UsuarioController@editar');
$router->post('/usuario-apagar', 'usuario@UsuarioController@apagar');
$router->get('/usuario-detalhe', 'usuario@UsuarioController@detalhe');
$router->get('/usuario-alterar-senha','usuario@UsuarioController@editarSenha');
$router->post('/usuario-alterar-senha','usuario@UsuarioController@editarSenha');
$router->get('/usuario-meus-dados', 'usuario@UsuarioController@meusDados');
$router->post('/usuario-meus-dados', 'usuario@UsuarioController@meusDados');
$router->post('/usuario-update-relacionamento', 'usuario@UsuarioController@updateRelacionamento');
$router->get('/usuario-dados-cliente', 'usuario@UsuarioController@dadosCliente');
$router->post('/usuario-dados-cliente', 'usuario@UsuarioController@dadosCliente');

$router->get('/orcamento', 'orcamento@OrcamentoController@index');
$router->get('/orcamento-pesquisa', 'orcamento@OrcamentoController@pesquisa');
$router->get('/orcamento-cliente', 'orcamento@OrcamentoController@pesquisa');
$router->get('/orcamento-pdf', 'orcamento@OrcamentoController@documentoPDF');
$router->get('/orcamento-excel', 'orcamento@OrcamentoController@documentoEXCEL');
$router->post('/orcamento-cliente-usuaro', 'orcamento@OrcamentoController@orcamentoCliente');

$router->get('/acesso-negado', 'error@ErrorController@permissaoNegada');

