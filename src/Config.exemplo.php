<?php

namespace src;

class Config {
    
    const BASE_DIR = '/public';

    const DB_DRIVER = 'mysql';
    const DB_HOST = '';
    const DB_DATABASE = '';
    const DB_USER = '';
    const DB_PASS = '';

    const ERROR_CONTROLLER = 'ErrorController';
    const DEFAULT_ACTION = 'index';
    const PATH_CONTROLLER = 'error';

    const SESSION_TIMEOUT_MIN = 15; // If not set, default value is 15

    const SECRET_KEY = "<SECRET KEY>";

    const EMAIL = [
        "BACKEND" => "console", // Choose "console" or "smtp"
        "HOST"    => "<MAIL SERVER>",
        "PORT"    => "<MAIL PORT>",
        "USER"    => "<MAIL USER>",
        "PASS"    => "<MAIL PASS>",
        "TLS"     => false
    ];
}
