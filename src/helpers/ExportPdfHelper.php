<?php

namespace src\helpers;

use Dompdf\Dompdf;
use Dompdf\Options;
use Exception;
use src\models\OrcamentoDAO;

class ExportPdfHelper 
{
    private static $idCliente = null;
    private static $idPedido = null;
    private static $setor = null;

    private static function construct()
    {
        self::$idCliente = filter_input(INPUT_GET, 'idCliente', FILTER_SANITIZE_SPECIAL_CHARS);
        self::$idPedido = filter_input(INPUT_GET, 'idPedido', FILTER_SANITIZE_SPECIAL_CHARS);
        self::$setor = filter_input(INPUT_GET, 'setor', FILTER_SANITIZE_SPECIAL_CHARS);

        $x = explode(' - ', self::$setor);
        self::$setor = $x[0];
    }

    public static function getPdf()
    {
        self::construct();

        switch(self::$setor){
            case 2:
                self::getSolo();
            break;
            case 3:
                self::getFoliar();
            break;
            case 9:
                self::getCalcario();
            break;
        }
    }

    private static function getFoliar()
    {
        try{
            $model = new OrcamentoDAO();
            $template_html = FunctionHelper::getTemplateSystem("model_foliar");
            $template_html = self::setHeader($template_html, 'RESULTADO DE ANÁLISE DE FOLIAR');

            $dados_resultado = $model->selectAnaliseResultado(self::$idPedido);

            $k = explode('-', $dados_resultado[0]["Amo_Identificacao_a"]);
            $chave = $k[1];
            $array = [];
            $item = [];
            foreach($dados_resultado as $k => $value){
                $k = explode('-', $value["Amo_Identificacao_a"]);

                if($chave === $k[1]){
                    $item["amostra"] = $chave . ' | ' . $value["Amo_Descricao_a"] . ' | ' . $value["Descricao_a"];
                    $item[$value["DescServOrcamento_a"]] = FunctionHelper::decimalUSDtoBRA($value["resultadofinal"]);
                }else{
                    $array[] = $item;

                    $chave = $k[1];
                    $item = [];

                    $item[$value["DescServOrcamento_a"]] = FunctionHelper::decimalUSDtoBRA($value["resultadofinal"]);
                }
            }
            
            $array[] = $item;

            $html = "";
            foreach($array as $k => $value){
                $N = $value["N"] ?? '';
                $P = $value["P"] ?? '';
                $K = $value["K"] ?? '';
                $CA = $value["Ca"] ?? '';
                $MG = $value["Mg"] ?? '';
                $S = $value["S"] ?? '';
                $NA = $value["Na"] ?? '';
                $B = $value["B"] ?? '';
                $CU = $value["Cu"] ?? '';
                $FE = $value["Fe"] ?? '';
                $MN = $value["Mn"] ?? '';
                $ZN = $value["Zn"] ?? '';
                $MO = $value["Mo"] ?? '';

                $html .= "<tr>";
                $html .= "<td colspan='14' class='font-size-result'><strong>".$value["amostra"]."</strong></td>";
                $html .= "</tr>";
                $html .= "<tr>";
                $html .= "<td></td>";
                $html .= "<td class='font-size-result'>".$N."</td>";
                $html .= "<td class='font-size-result'>".$P."</td>";
                $html .= "<td class='font-size-result'>".$K."</td>";
                $html .= "<td class='font-size-result'>".$CA."</td>";
                $html .= "<td class='font-size-result'>".$MG."</td>";
                $html .= "<td class='font-size-result'>".$S."</td>";
                $html .= "<td class='font-size-result'>".$NA."</td>";
                $html .= "<td class='font-size-result'>".$B."</td>";
                $html .= "<td class='font-size-result'>".$CU."</td>";
                $html .= "<td class='font-size-result'>".$FE."</td>";
                $html .= "<td class='font-size-result'>".$MN."</td>";
                $html .= "<td class='font-size-result'>".$ZN."</td>";
                $html .= "<td class='font-size-result'>".$MO."</td>";
                $html .= "</tr>";
            }

            $template_html = str_replace("{RESULTADO}", $html, $template_html);

            self::outputExport($template_html);
            
        }catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private static function getCalcario()
    {
        try{
            $model = new OrcamentoDAO();
            $template_html = FunctionHelper::getTemplateSystem("model_calcario");
            $template_html = self::setHeader($template_html, 'RESULTADO DE ANÁLISE CALCARIO');

            $dados_resultado = $model->selectAnaliseResultado(self::$idPedido);

            // echo '<pre>';
            // var_dump($dados_resultado);
            // exit;

            $k = explode('-', $dados_resultado[0]["Amo_Identificacao_a"]);
            $chave = $k[1];
            $array = [];
            $item = [];
            foreach($dados_resultado as $k => $value){
                $k = explode('-', $value["Amo_Identificacao_a"]);

                if($chave === $k[1]){
                    $item[$value["ser_servico_a"]] = FunctionHelper::decimalUSDtoBRA($value["resultadofinal"]);
                }else{
                    $array[] = $item;

                    $chave = $k[1];
                    $item = [];

                    $item[$value["ser_servico_a"]] = FunctionHelper::decimalUSDtoBRA($value["resultadofinal"]);
                }
            }
            
            $array[] = $item;

            $html = "";
            while ($array = current($item)) {

                $html .=   "<tr>
                                <td class='font-size-result'>".key($item)."</td>
                                <td>..............................................</td>
                                <td class='font-size-result'>{$array}</td>
                                <td class='font-size-result'> % </td>
                            </tr>";

                next($item);
            }

            // echo $html;
            // exit;

            $template_html = str_replace("{RESULTADO}", $html, $template_html);

            self::outputExport($template_html);

        }catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private static function getSolo()
    {
        try{
            $model = new OrcamentoDAO();
            $template_html = FunctionHelper::getTemplateSystem("model_solo");
            $template_html = self::setHeader($template_html, 'RESULTADO DE ANÁLISE DE SOLO');

            $dados_resultado = $model->selectAnaliseResultado(self::$idPedido);

            // echo '<pre>';
            // var_dump($dados_resultado);
            // exit;

            $k = explode('-', $dados_resultado[0]["Amo_Identificacao_a"]);
            $chave = $k[1];
            $array = [];
            $item = [];
            foreach($dados_resultado as $k => $value){
                $k = explode('-', $value["Amo_Identificacao_a"]);

                if($chave === $k[1]){
                    $item["amostra"] = $chave . ' | ' . $value["Amo_Descricao_a"] . ' | ' . $value["talhao"] . ' | ' . $value["Descricao_a"];
                    $item[$value["DescServOrcamento_a"]] = FunctionHelper::decimalUSDtoBRA($value["resultadofinal"]);
                }else{
                    $array[] = $item;

                    $chave = $k[1];
                    $item = [];

                    $item[$value["DescServOrcamento_a"]] = FunctionHelper::decimalUSDtoBRA($value["resultadofinal"]);
                }
            }
            
            $array[] = $item;

        //    echo '<pre>';
        //    var_dump($array);
        //    exit;

           $html = "";
            foreach($array as $k => $value){
                $pH = $value["pH  (CaCl2)"] ?? '';
                $MO = $value["MO"] ?? '';
                $P = $value["P (RESINA)"] ?? '';
                $K = $value["K (NH4Cl)"] ?? '';
                $Ca = $value["Ca (NH4CL)"] ?? '';
                $Mg = $value["Mg (NH4CL)"] ?? '';
                $HAl = $value["H+Al"] ?? '';
                $Al = $value["Al"] ?? '';
                $S = $value["S"] ?? '';
                $Na = $value["Na"] ?? '';
                $SB = $value["SB"] ?? '';
                $CTC = $value["CTC"] ?? '';
                $V = $value["V%"] ?? '';
                $M = $value["m"] ?? '';
                $B = $value["B"] ?? '';
                $Cu = $value["Cu (DTPA)"] ?? '';
                $Fe = $value["Fe (DTPA)"] ?? '';
                $Mn = $value["Mn (DTPA)"] ?? '';
                $Zn = $value["Zn (DTPA)"] ?? '';

                $html .= "<tr>";
                $html .= "<td colspan='14' class='font-size-result'><strong>".$value["amostra"]."</strong></td>";
                $html .= "</tr>";
                $html .= "<tr>";
                $html .= "<td></td>";
                $html .= "<td class='font-size-result'>".$pH."</td>";
                $html .= "<td class='font-size-result'>".$MO."</td>";
                $html .= "<td class='font-size-result'>".$P."</td>";
                $html .= "<td class='font-size-result'>".$K."</td>";
                $html .= "<td class='font-size-result'>".$Ca."</td>";
                $html .= "<td class='font-size-result'>".$Mg."</td>";
                $html .= "<td class='font-size-result'>".$HAl."</td>";
                $html .= "<td class='font-size-result'>".$Al."</td>";
                $html .= "<td class='font-size-result'>".$S."</td>";
                $html .= "<td class='font-size-result'>".$Na."</td>";
                $html .= "<td class='font-size-result'>".$SB."</td>";
                $html .= "<td class='font-size-result'>".$CTC."</td>";
                $html .= "<td class='font-size-result'>".$V."</td>";
                $html .= "<td class='font-size-result'>".$M."</td>";
                $html .= "<td class='font-size-result'>".$B."</td>";
                $html .= "<td class='font-size-result'>".$Cu."</td>";
                $html .= "<td class='font-size-result'>".$Fe."</td>";
                $html .= "<td class='font-size-result'>".$Mn."</td>";
                $html .= "<td class='font-size-result'>".$Zn."</td>";
                $html .= "</tr>";
            }

            $template_html = str_replace("{RESULTADO}", $html, $template_html);

            // echo $html;exit;

            self::outputExport($template_html);

        }catch (Exception $e) {
            echo $e->getMessage();
        }
    }
        
    private static function setHeader($template_html, $title){
        //logo pdf
        $path = FunctionHelper::getBaseUrl().'/assets/img/logoRibersolo.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/'.$type.';base64,'.base64_encode($data);

        //iac pdf
        $path = FunctionHelper::getBaseUrl().'/assets/img/iac.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base642 = 'data:image/'.$type.';base64,'.base64_encode($data);

        //selo pdf
        $path = FunctionHelper::getBaseUrl().'/assets/img/selo.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base643 = 'data:image/'.$type.';base64,'.base64_encode($data);

        $template_html = str_replace("{TITLE}", $title, $template_html);
        $template_html = str_replace("{LOGO}", $base64, $template_html);
        $template_html = str_replace("{IAC}", $base642, $template_html);
        $template_html = str_replace("{SELO}", $base643, $template_html);
        $template_html = str_replace("{DATEHOUR}", FunctionHelper::dateHourEUAtoBRA(FunctionHelper::currentTimeDate()), $template_html);
    
        $model = new OrcamentoDAO();
        $dados_cliente = $model->selectDadoCliente(self::$idPedido);

        $template_html = str_replace("{NUM_PEDIDO}", $dados_cliente["Orc_CodOrcamento"], $template_html);
        $template_html = str_replace("{DATA_APROVACAO}", $dados_cliente["orc_dataconfirmacao_d"], $template_html);
        $template_html = str_replace("{DATA_SAIDA}", $dados_cliente["DATASAIDA"], $template_html);
        $template_html = str_replace("{NUM_INICIO}", "", $template_html);
        $template_html = str_replace("{NUM_FIM}", "", $template_html);
        $template_html = str_replace("{PROPRIETARIO}", $dados_cliente["cli_nomecliente_a"], $template_html);
        $template_html = str_replace("{PROPRIEDADE}", "", $template_html);
        $template_html = str_replace("{MUNICIPIO}", "", $template_html);
        $template_html = str_replace("{END_CORRESPONDENCIA}", $dados_cliente["Endereco_a"], $template_html);
        $template_html = str_replace("{NUM_CORRESPONDENCIA}", $dados_cliente["Numero"], $template_html);
        $template_html = str_replace("{COMPLEMENTO_CORRESPONDENCIA}", $dados_cliente["Complemento"], $template_html);
        $template_html = str_replace("{BAIRRO_CORRESPONDENCIA}", $dados_cliente["Bairro_a"], $template_html);
        $template_html = str_replace("{CEP_CORRESPONDENCIA}", $dados_cliente["CEP_a"], $template_html);
        $template_html = str_replace("{CIDADE_CORRESPONDENCIA}", $dados_cliente["Cidade_a2"], $template_html);
        $template_html = str_replace("{UF_CORRESPONDENCIA}", "", $template_html);

        return $template_html;
    }

    private static function outputExport($template_html)
    {
        $dompdf = new Dompdf();
    
        ob_start();
        $dompdf->loadHtml($template_html);
        $dompdf->setPaper('A4', 'portraid');
        $dompdf->render();
        ob_end_clean();
        
        header('Content-type: application/pdf');
        echo $dompdf->output();
    }

}