<?php
namespace src\helpers;

use src\models\UsuarioDAO;
use src\models\UsuarioEntity;

class LoginHelper {

    public static function isAuthenticatedUser(){
        if(isset($_SESSION['token']) && isset($_SESSION['key'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function getInfoUser() {
        if (isset($_SESSION['key'])) {
            $usuarioDAO = new UsuarioDAO();
            return new UsuarioEntity($usuarioDAO->selectId($_SESSION['key']));
        }
        return null;
    }

    public static function doLogin($login, $senha){

        $usuarioDAO = new UsuarioDAO();

        if(LoginHelper::isEmail($login)){
            $usuario = $usuarioDAO->selectEmail($login);
        }else{
            $usuario = $usuarioDAO->selectCpfCnpj($login);
        }

        if(!empty($usuario)){

            $senha = md5($senha);

            if($senha == $usuario['senha']){
                $token = md5(time().rand(0,9999).time());
                $usuarioDAO->updateToken($token, $usuario['idUsuario']);
                $_SESSION['token'] = $token;
                $_SESSION['key'] = $usuario['idUsuario'];
                $_SESSION['ADM'] = $usuario['tipoUsuario'] == 'ADM' ? true : false;
                self::loginFailure_Reset($login);
                return $token;
            } else {
                self::loginFailure_Increment($login);
            }
        }

        return false;
    }

    public static function checkedStatus($login, $senha)
    {
        $usuarioDAO = new UsuarioDAO();
        if(LoginHelper::isEmail($login)){
            return $usuarioDAO->checkedStatusEmail($login);
        }else{
            return $usuarioDAO->checkedStatusCpfCnpj($login);
        }
    }

    public static function doLoginCheckedModal($login, $senha){

        $usuarioDAO = new UsuarioDAO();

        if(LoginHelper::isEmail($login)){
            $usuario = $usuarioDAO->selectEmail($login);
        }else{
            $usuario = $usuarioDAO->selectCpfCnpj($login);
        }

        if(!empty($usuario)){
            return $usuario;
        }

        return false;
    }

    public static function doLogout($session_destroy = true) {
        if ($session_destroy) {
            session_destroy();
        } else {
            session_unset();
        }
    }

    public static function loginFailure_ThresholdExceeded($user) {
        $failure_threshold = 5;

        /*
        $f = MC::get("__login_failure_".$user);
        if ($f && ($f >= $failure_threshold)) {
            return true;
        }
        return false;*/
    }

    public static function loginFailure_GetAttempt($user) {
        /* return MC::get("__login_failure_".$user); */
    }

    public static function loginFailure_Increment($user) {
        $failure_expiration = 7*24*60*60; // 7 days

        /*
        if (self::loginFailure_GetAttempt($user)) {
            MC::inc("__login_failure_".$user);
        } else {
            MC::set("__login_failure_".$user, 1, $failure_expiration);
        }*/
    }

    public static function loginFailure_Reset($user) {
        /* MC::del("__login_failure_".$user); */
    }

    public static function loginFailure_ResetById($id) {
        $usuarioDAO = new UsuarioDAO();
        
        /*
        $user = $usuarioDAO->selectIdUsuario($id);
        if (!empty($user)) {
            self::loginFailure_Reset($user->getLoginUsuario());
        } */
    }

    private static function isEmail($login){
        if (!filter_var($login, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }

}