<?php

namespace src\helpers;

use NumberFormatter;
use src\api\JsonAjax;
use src\Config;
use src\models\UsuarioEntity;
use stdClass;

class FunctionHelper
{
    /**
     * Função para transformar string em upperCase FORMATO UTF+8
     * 
     * @param valor             Valor a ser transformado
     */
    public static function upperCase($valor){
        mb_internal_encoding('UTF-8');
        return mb_strtoupper($valor);
    }

    /**
     * Função para transformar uma data formato americano em formato brasileiro
     * 
     * @param valor                 valor a ser transformado
     */
    public static function dateHourEUAtoBRA($valor){
        if ($valor == "0000-00-00" || $valor == "") {
            return "-";
        } else {
            if (strlen($valor) == "10") {
                $lista_array = explode("-", $valor);
                return $lista_array[2] . "/" . $lista_array[1] . "/" . $lista_array[0];
            }
            if (strlen($valor) == "7") {
                $lista_array = explode("-", $valor);
                return $lista_array[1] . "/" . $lista_array[0];
            }
            if (strlen($valor) == "19") {
                $lista_array_separahora = explode(" ", $valor);
                $lista_array = explode("-", $lista_array_separahora[0]);
                return $lista_array[2] . "/" . $lista_array[1] . "/" . $lista_array[0] . " " . $lista_array_separahora[1];
            }
        }
    }

    /**
     * Formata o valor USD para o formato USD
     * 
     * @param $valor        valor a ser formatado
     * @param $minDecimal   valor minimo para a casa decimal
     * @param $maxDecimal   valor maximo de casas decimal para a formatação
     */
    public static function decimalUSDtoUSD($valor, $maxDecimal = 2, $minDecimal = 2){
        $formatter = new NumberFormatter('en-US',  \NumberFormatter::DECIMAL);
        $formatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, $minDecimal);
        $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, $maxDecimal); 
        return $formatter->format($valor);
    }

    /**
     * Formata o valor BRA para valor USD para inserir na base de dados
     * 
     * @param $valor        valor a ser formatado
     * @param $minDecimal   valor minimo para a casa decimal
     * @param $maxDecimal   valor maximo de casas decimal para a formatação
     */
    public static function decimalBRAtoUSD($valor, $maxDecimal = 2, $minDecimal = 2){
        $formatter = new NumberFormatter('pt-BR',  \NumberFormatter::DECIMAL);
        $formatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, $minDecimal);
        $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, $maxDecimal); 
        return $formatter->parse($valor); 
    }

    /**
     * Formata o valor USD para o formato BRA 
     * 
     * @param $valor        valor a ser formatado
     * @param $minDecimal   valor minimo para a casa decimal
     * @param $maxDecimal   valor maximo de casas decimal para a formatação
     */
    public static function decimalUSDtoBRA($valor, $maxDecimal = 2, $minDecimal = 2){
        $formatter = new NumberFormatter('pt-BR',  \NumberFormatter::DECIMAL);
        $formatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, $minDecimal);
        $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, $maxDecimal); 
        return $formatter->format($valor);
    }

    /**
     * Retorna uma data hora atual
     */
    public static function currentTimeDate(){
        return date('Y-m-d H:i:s');
    }

     /**
     * Retorna uma data atual
     */
    public static function currentDate(){
        return date('Y-m-d');
    }

    /**
     * Retorna a hora atual
     */
    public static function currentHour(){
        return date('H:i:s');
    }

    /**
     * Fução retorna somente numeros
     */
    public static function onlyNumber($value){
        return preg_replace("/[^0-9]/", "", $value);
    }

    public static function removeAccentuation($value){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$value);
    }

    /**
     * Função para verificar se o usuario é cliente ou convenio
     * 
     * @return true            Cliente
     * @return false           Convenio
     */
    public static function checkedUser(UsuarioEntity $obj){
        if($obj->getIdCliente() > 0){
            return true;
        }
        return false;
    }

    /**
     * Função para encriptar valuees
     * 
     * @param value                 value a ser encriptorafado
     */
    public static function aesEncriptar($value){
        return bin2hex(openssl_encrypt($value, 'aes-256-cbc', substr(Config::SECRET_KEY, 0, 32), OPENSSL_RAW_DATA, substr(Config::SECRET_KEY, 0, 16))); 
    }

    /**
     * Função para desencriptar valuees
     * 
     * @param value                 value a ser desencriptado
     */
    public static function aesDesencriptar($value){
        return openssl_decrypt(hex2bin($value), 'aes-256-cbc', substr(Config::SECRET_KEY, 0, 32), OPENSSL_RAW_DATA, substr(Config::SECRET_KEY, 0, 16));
    }

    /**
     * Função retorna a url base
     */
    public static function getBaseUrl() {
        $base = (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') ? 'https://' : 'http://';
        $base .= $_SERVER['SERVER_NAME'];
        if($_SERVER['SERVER_PORT'] != '80') {
            $base .= ':'.$_SERVER['SERVER_PORT'];
        }
        $base .= Config::BASE_DIR;

        return $base;
    }

    public static function getTemplateSystem($templateName)
    {
        return self::getTemplate($templateName);
    }

    private static function getTemplate($templateName)
    {
        if (file_exists('../templates/models/'.$templateName.'.php')) {

            ob_start();
            require '../templates/template_base.php';
            $dump = ob_get_contents();
            ob_end_clean();

            return $dump;

        } else {
            JsonAjax::response(false, "Template não encontrado.", "Erro processo de geração de relatório.");
        }
    }
}