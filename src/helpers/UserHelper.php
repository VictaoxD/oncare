<?php

namespace src\helpers;

use core\Entity;
use src\models\ClienteDAO;
use src\models\ConvenioDAO;

class UserHelper extends Entity
{
    public static function getClients()
    {
        $modelCliente = new ClienteDAO();

        $arrayClient = [];
        $user = LoginHelper::getInfoUser();

        if(empty($user->getClientes()) && empty($user->getConvenios())){
            return [];
        }

        if(empty($user->getClientes())){
            $user->setClientes(0);
        }

        if(empty($user->getConvenios())){
            $user->setConvenios(0);
        }
        
        $arrayClient = $modelCliente->consultarClientes($user);

        return $arrayClient;
    }

    public static function getMeusDados()
    {
        $user = LoginHelper::getInfoUser();

        if(empty($user->getClientes()) && empty($user->getConvenios())){
            return [];
        }

        if(!empty($user->getClientes())){
            $model = new ClienteDAO();
            return $model->consultarMeusDados($user->getIdCliente());
        }

        if(!empty($user->getConvenios())){
            $model = new ConvenioDAO();
            return $model->consultarMeusDados($user->getIdConvenio());
        }
    }

    public static function getDadosCliente()
    {
        $user = LoginHelper::getInfoUser();

        if(empty($user->getClientes()) && empty($user->getConvenios())){
            return [];
        }

        if(!empty($user->getClientes())){
            $model = new ClienteDAO();
            return $model->consultarMeusDados($user->getIdCliente());
        }

        if(!empty($user->getConvenios())){
            $model = new ConvenioDAO();
            return $model->consultarMeusDados($user->getIdConvenio());
        }
    }
}