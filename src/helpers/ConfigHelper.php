<?php

namespace src\helpers;

use config\ConfigSystem;

class ConfigHelper{

    public static function getConfig(){
        $data['menu'] = ConfigSystem::MENU;
        $data['config'] = ConfigSystem::CONFIG;

        if(isset($_SESSION['ADM'])){
            $newData['config'] = ConfigSystem::CONFIG;
            foreach($data['menu'] as $value){
                if($value['identification'] !== 'usuario'){
                    $newData['menu'][] = $value;
                }  
            }

            return $newData;
        }

        return $data;
    }

}