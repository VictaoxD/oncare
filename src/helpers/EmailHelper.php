<?php

namespace src\helpers;

use \PHPMailer\PHPMailer\PHPMailer;
use \PHPMailer\PHPMailer\SMTP;
use \PHPMailer\PHPMailer\Exception;
use \stdClass;
use \src\Config;

class EmailHelper{

    private $mail;

    private $data;

    private $error;

    private $backend;

    public function __construct()
    {
        $this->mail = new PHPMailer();
        $this->data = new stdClass();
        $this->backend = "console";

        $this->mail->isSMTP();
        $this->mail->isHTML();
        $this->mail->setLanguage($langcode = "br");

        $this->mail->SMTPAuth = true;
        $this->mail->CharSet = PHPMailer::CHARSET_UTF8;

        if (defined('src\Config::EMAIL')) {
            $this->mail->Host = Config::EMAIL["HOST"];
            $this->mail->Port = Config::EMAIL["PORT"];
            $this->mail->Username = Config::EMAIL["USER"];
            $this->mail->Password = Config::EMAIL["PASS"];

            if (Config::EMAIL["TLS"] === true) {
                $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            } else {
                $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            }

            if ((Config::EMAIL["BACKEND"] === "console") ||
                (Config::EMAIL["BACKEND"] === "smtp")) {
                $this->backend = Config::EMAIL["BACKEND"];
            } else {
                error_log("WARN: Email backend '".Config::EMAIL["BACKEND"]."'".
                          " is not supported. The 'console' backend was set.");
            }
        }
    }

    public function attach(string $filePath, string $fileName) : EmailHelper
    {
        $this->data->attach[$filePath] = $fileName;
        return $this;
    }

    public function send(string $from_mail, string $recipient_mail,
                         string $subject, string $body) : bool
    {
        if ($this->backend === "console") {
            $log_msg = "-----------------------------\n".
                       "From: $from_mail\n".
                       "To: $recipient_mail\n".
                       "Subject: $subject\n".
                       "-----------------------------\n".
                       "$body\n".
                       "-----------------------------\n";

            file_put_contents('php://stderr', $log_msg);
            return true;
        }

        try{
            $this->mail->Subject = $subject;
            $this->mail->msgHTML(nl2br($body));
            $this->mail->addAddress($recipient_mail);
            $this->mail->setFrom($from_mail);

            if(!empty($this->data->attach)){
                foreach($this->data->attach as $path => $name){
                    $this->mail->addAttachment($path, $name);
                }
            }

            $this->mail->send();
            return true;
        }catch(Exception $e){
            $this->error = $e;
            return false;
        }
    }

    public function error()
    {
        return $this->error;
    }
}
