<?php

namespace src\helpers;

class ErrorHelper
{
    /**
     * Função para setar o erro dentro da session
     * 
     * @param arraySettings                 Variaveis de configuração da mensagem de erro
     * @param arrayError                    Array com os erros
     */
    public static function setSessionError($arraySettings, $arrayError){
        if(isset($_SESSION['error']) && !empty($_SESSION['error'])){
            //pega erros ja existentes na session
            $item = $_SESSION['error']['item'];

            foreach($arrayError as $value){
                array_push($item, $value);
            }

            //adiciona os erros já existentes e os novos
            $_SESSION['error']['item'] = $item;

        }else{
            //cria a session error
            $_SESSION['error'] = [ 
                'status' => $arraySettings["status"],
                'title' => $arraySettings["title"],
                'color' => $arraySettings["color"],
                'item' => $arrayError
            ];
        }

    }

    /**
     * Funcao retornar os erros armazenados na session em forma de array ou json
     * 
     * @param json        Identifica se o retorno vai ser um json ou um array
     */
    public static function getSessionError($json){
        if(isset($_SESSION['error']) && !empty($_SESSION['error'])){
            $error = $_SESSION['error'];
            $_SESSION['error'] = '';
            if($json){
                return json_encode($error);
            }else{
                return $error;
            }
        }
        return;
    }

    /**
     * Verifica se a session error existe e é diferente de vazia
     */
    public static function hasSessionError(){
        if(isset($_SESSION['error']) && !empty($_SESSION['error'])){
            return true;
        }
        return false;
    }

}