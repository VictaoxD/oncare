<?php

namespace src\helpers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use src\models\OrcamentoDAO;

class ExportExcelHelper
{
    private static $idCliente = null;
    private static $idPedido = null;
    private static $setor = null;

    private static function construct()
    {
        self::$idCliente = filter_input(INPUT_GET, 'idCliente', FILTER_SANITIZE_SPECIAL_CHARS);
        self::$idPedido = filter_input(INPUT_GET, 'idPedido', FILTER_SANITIZE_SPECIAL_CHARS);
        self::$setor = filter_input(INPUT_GET, 'setor', FILTER_SANITIZE_SPECIAL_CHARS);

        $x = explode(' - ', self::$setor);
        self::$setor = $x[0];
    }

    public static function getExcel()
    {
        self::construct();

        switch(self::$setor){
            case 2:
                self::getCsv("Planilha_solo.csv");
            break;
            case 3:
                self::getCsv("Planilha_foliar.csv");
            break;
            case 9:
                self::getCsv("Planilha_calcario.csv");
            break;
        }
    }

    public static function getCsv($title)
    {
        $fileName = $title;

        $data = self::getData();
        $data = self::getRows($data);

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$fileName.'";');

        $f = fopen('php://output', 'w');

        foreach($data as $row) {
            fputcsv($f, $row, ';');
        }

        exit;
        fclose($f);

        exit();
    }

    private static function getData()
    {
        $model = new OrcamentoDAO();
        $dados_resultado = $model->selectAnaliseResultadoCsv(self::$idPedido);

        $k = explode('-', $dados_resultado[0]["Amo_Identificacao_a"]);
        $chave = $k[1];
        $array = [];
        $item = [];
        foreach($dados_resultado as $k => $value){
            $k = explode('-', $value["Amo_Identificacao_a"]);

            if($chave === $k[1]){
                $item["codlab"] = $k[1];
                $item["dataentrada"] = $value["dataentrada"];
                $item["datasaida"] = $value["datasaida"];
                $item["solicitante"] = "";
                $item["proprietario"] = $value["proprietario"];
                $item["propriedade"] = "";
                $item["municiouf"] = "";
                $item["codinteressado"] = $value["Amo_Descricao_a"];
                $item["cultura"] = $value["Descricao_a"];
                $item["cultura_dris"] = "";
                $item["partedaplanta"] = "";
                $item[$value["ser_servico_a"]] = FunctionHelper::decimalUSDtoUSD($value["resultadofinal"]);
            }else{
                $array[] = $item;

                $chave = $k[1];
                $item = [];

                $item["codlab"] = $k[1];
                $item["dataentrada"] = $value["dataentrada"];
                $item["datasaida"] = $value["datasaida"];
                $item["solicitante"] = "";
                $item["proprietario"] = $value["proprietario"];
                $item["propriedade"] = "";
                $item["municiouf"] = "";
                $item["codinteressado"] = $value["Amo_Descricao_a"];
                $item["cultura"] = $value["Descricao_a"];
                $item["cultura_dris"] = "";
                $item["partedaplanta"] = "";
                $item[$value["ser_servico_a"]] = FunctionHelper::decimalUSDtoUSD($value["resultadofinal"]);
            }
        }

        $array[] = $item;
        return $array;
    }

    private static function getRows($data)
    {
        $array[] = self::getHeader($data[0]);

        $item = [];
        foreach($data as $value){
            $item = [];
            foreach($value as $v){
                $item[] = FunctionHelper::removeAccentuation($v);
            }
            $array[] = $item;
        }

        return $array;
    }

    private static function getHeader($data)
    {
        $item = [];
        foreach($data as $k => $v){
            $item[] = FunctionHelper::upperCase(FunctionHelper::removeAccentuation($k));
        }

        return $item;
    }

}