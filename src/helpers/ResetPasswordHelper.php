<?php

namespace src\helpers;

use src\models\UsuarioDAO;
use src\Config;

class PasswordResetTokenGenerator {

    private $secret = Config::SECRET_KEY;

    // Timeout in seconds
    private $reset_timeout = 2*60*60;

    public function makeToken($usuario) {
        return $this->makeTokenWithTimestamp($usuario, time());
    }

    public function checkToken($usuario, $token) {
        if ((!$usuario) || (!$token)) {
            return false;
        }

        $ts_b36 = explode('-', $token)[0];
        $ts = base_convert($ts_b36, 36, 10);

        // hash_equals is timing attack safe
        if (!hash_equals($this->makeTokenWithTimestamp($usuario, $ts), $token)) {
            return false;
        }

        // check the timeout
        if ((time() - $ts) > $this->reset_timeout) {
            return false;
        }

        return true;
    }

    private function makeTokenWithTimestamp($usuario, $ts) {
        $ts_b36 = base_convert($ts, 10, 36);
        $hash_str = hash_hmac("sha256", $this->makeHashValue($usuario, $ts), $this->secret);

        // Limit to 20 characters to shorten the URL
        $hash_str = substr($hash_str, 0, 20);

        return $ts_b36."-".$hash_str;
    }

    private function makeHashValue($usuario, $ts) {
        // The hash combines four values:
        //     The user's primary key
        //     The user's hashed password
        //     The user's last login timestamp
        //     The current timestamp ($ts)

        $last_login = $usuario['ultimoLogin'];
        if (!$last_login) {
            $last_login = "";
        }
        return $usuario['idusuario'].
               $usuario['senha'].
               $last_login.$ts;
    }
}

class ResetPasswordHelper {

    private static function sendRecoveryEmail($usuario) {
        $email_helper = new EmailHelper();
        $token_generator = new PasswordResetTokenGenerator();

        $from = "admin@sysconag.com.br";
        $to = $usuario['email'];
        $subject = "Solicitação de Alteração de Senha";

        $nome = $usuario['nome'];
        $cpf = $usuario['cpf'];
        $protocol = $_SERVER['REQUEST_SCHEME'];
        $domain = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        $uid = base64_encode($usuario['idusuario']);
        $token = $token_generator->makeToken($usuario);
        $url = "reset?reset_token=$uid/$token";

        $message =
            "Olá $nome, tudo bom?\n\n".

            "Você solicitou uma mudança de senha. Seu cpf é $cpf.\n".
            "Para prosseguir, clique no link abaixo e configure sua nova senha.\n\n".

            "$protocol://$domain/$url\n\n".

            "Att,\n".
            "Equipe Sysconag";

        $email_helper->send($from, $to, $subject, $message);
    }

    public static function getUserIdFromToken($reset_token) {
        $uid = explode('/', $reset_token)[0];
        return base64_decode($uid);
    }

    public static function resetPasswordPhase1($email) {
        $usuarioDAO = new UsuarioDAO();
        $usuario = $usuarioDAO->selectEmail($email);

        if (!empty($usuario)) {
            ResetPasswordHelper::sendRecoveryEmail($usuario);
            return true;
        }

        return false;
    }

    public static function resetPasswordPhase2($reset_token) {
        $usuarioDAO = new UsuarioDAO();
        $token_generator = new PasswordResetTokenGenerator();

        if (!$reset_token) {
            return false;
        }

        $uid = ResetPasswordHelper::getUserIdFromToken($reset_token);
        $token = explode('/', $reset_token)[1];
        $usuario = $usuarioDAO->selectId($uid);

        if (!$token_generator->checkToken($usuario, $token)) {
            return false;
        }

        return true;
    }

    public static function resetPasswordPhase3($uid, $password) {
        if ($uid && $password) {
            $usuarioDAO = new UsuarioDAO();
            $usuarioDAO->updatePassword($uid, $password);
            return true;
        }

        return false;
    }
}

