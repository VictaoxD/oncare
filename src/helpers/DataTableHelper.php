<?php

namespace src\helpers;

use config\ConfigSystem;

class DataTableHelper 
{
    private static $field = null;
    private static $type = null;

    private static function construct()
    {
        if (isset($_GET["campo"]) && !empty($_GET["campo"])) {
            self::$field = FunctionHelper::aesDesencriptar(filter_input(INPUT_GET, 'campo', FILTER_SANITIZE_SPECIAL_CHARS));
        }

        if (isset($_GET["tipo"]) && !empty($_GET["tipo"])) {
            self::$type = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_SPECIAL_CHARS);
        }
    }

    public static function getHeader($identification, $idCliente)
    {
        if (empty($identification)) {
            return [];
        }
        
        self::construct();
        $configSystem = ConfigSystem::DATATABLE[$identification];

        $model = "src\models\\" . $configSystem["config"]["entity"] . "DAO";
        $model = new $model; 

        $header = [];
        $sql = "";
        $order = "";
        foreach($configSystem["datatable"] as $value){  
            $type = "desc";
            if ($value["field"] == self::$field) {
                if (self::$type == "asc") {
                    $type = "desc";
                }else{
                    $type = "asc";
                }
            }

            $header[] = [
                "title" => $value["title"],
                "link" => $value["link"]."?campo=".FunctionHelper::aesEncriptar($value["field"])."&tipo=".$type,
                "type" => null
            ];

            if (!empty($value["field"])) { 
                $sql .= $value["field"] . ",";
            }
        }

        $sql = substr($sql, 0, -1);

        if (empty(self::$field)) {
            $order .= $configSystem["datatable"][0]["field"] . " desc";
            $header[0]["type"] = "desc";
        } else {
            $order .= self::$field . " " . self::$type;

            foreach($configSystem["datatable"] as $k => $value){  
                if ($value["field"] == self::$field) {
                    $header[$k]["type"] = self::$type;
                }   
            }
        }

        $data = $model->selectGrid($sql, $order, $idCliente);

        return [
            "header" => $header,
            "data" => $data ?? []
        ];
        
    }

}