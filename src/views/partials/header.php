<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
	<div class="container d-flex align-items-center">
	<?php /*
	<h1 class="logo mr-auto"><a href="index.html">BizLand<span>.</span></a></h1>
	<!-- Uncomment below if you prefer to use an image logo --> */ ?>
	<a href="index.html" class="logo mr-auto"><img src="<?= $base; ?>/assets/img/logoOncare.png" alt=""></a>

			<nav class="nav-menu d-none d-lg-block" style="text-transform: uppercase;">
				<ul>
					<li><a href="<?= $base; ?>">Home</a></li>
					<li><a href="<?= $base; ?>/corpore">Corpore</a></li>
					<li class="drop-down"><a href="#">Parceiros</a>
						<ul>
							<li><a href="<?= $base; ?>/parceiros/empresas">Corporativo</a></li>
							<li><a href="<?= $base; ?>/parceiros/escolas">Educacional</a></li>
						</ul>
					</li>
					<li><a href="<?= $base; ?>/como_funciona">Como Funciona</a></li>
					<li><a href="<?= $base; ?>/login">Seu Espaço</a></li>
					<?php /* <li class="drop-down"><a href="">Drop Down</a>
						<ul>
							<li><a href="#">Drop Down 1</a></li>
							<li class="drop-down"><a href="#">Deep Drop Down</a>
								<ul>
									<li><a href="#">Deep Drop Down 1</a></li>
									<li><a href="#">Deep Drop Down 2</a></li>
									<li><a href="#">Deep Drop Down 3</a></li>
									<li><a href="#">Deep Drop Down 4</a></li>
									<li><a href="#">Deep Drop Down 5</a></li>
								</ul>
							</li>
							<li><a href="#">Drop Down 2</a></li>
							<li><a href="#">Drop Down 3</a></li>
							<li><a href="#">Drop Down 4</a></li>
						</ul>
					</li> */ ?>
					<li><a href="<?= $base; ?>/login_psicologo">Para Psicólogos</a></li>
				</ul>
			</nav><!-- .nav-menu -->
		</div>
</header><!-- End Header -->