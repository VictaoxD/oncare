<div class="modal-header">
    <h5 class="modal-title"><?= $settings["title"]; ?></h5>

    <a href="javascript:void()" class="export-pdf-modal" title="Imprimir">
        <div class="ml-3">
            <i class="fas fa-print"></i>
        </div>
    </a>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
</div>
<div class="alert-message-details ml-3 mr-3 mt-2"></div>

<div id="print-this" class="modal-body">
    <?= $this->render('pages/'.$settings["view"], $viewData); ?>
</div>
