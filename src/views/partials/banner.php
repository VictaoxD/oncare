<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
        <h1>Não somos como os outros</h1>
        <h1>sites de <span>Terapia Online.</span></h1>
        <br/>
        <h6>Não somos apenas uma plataforma online. Somos feitos de pessoas extremamente capacitadas.<br/>Aqui, você encontra todo o suporte e ferramentas de que precisa para ser mais feliz.</h6>
        <br/>
        <div class="d-flex">
            <a href="#about" class="btn-get-started scrollto">Agendar</a>&nbsp;&nbsp;
            <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox btn-watch-video" data-vbtype="video" data-autoplay="true"> Watch Video <i class="icofont-play-alt-2"></i></a> -->
            <a href="#about" class="btn-get-started scrollto">Não sei qual psicólogo escolher</a>
        </div>
    </div>
</section><!-- End Hero -->