<nav class="topnav navbar navbar-expand shadow justify-content-between justify-content-sm-start navbar-dark bg-conag" id="sidenavAccordion">
    <!-- Navbar Brand-->
    <!-- * * Tip * * You can use text or an image for your navbar brand.-->
    <!-- * * * * * * When using an image, we recommend the SVG format.-->
    <!-- * * * * * * Dimensions: Maximum height: 32px, maximum width: 240px-->
    <a class="ml-2 mb-1"  href="<?= $base; ?>"><img src="<?= $base . $config['config']['logo']['path']; ?>" width="<?= $config['config']['logo']['width']; ?>" height="<?= $config['config']['logo']['height']; ?>" alt="<?= $config['config']['logo']['alt']; ?>"></a>
    <!-- Sidenav Toggle Button-->
    <button class="btn ml-2 btn-icon btn-transparent-dark order-1 order-lg-0 mr-lg-2" id="sidebarToggle"><i class="fas fa-bars"></i></button>
   
    <!-- Data Hora Sistema -->
    <div class="navbar-date-container d-none d-sm-block">
		<div id="date-display"><?= $date; ?></div>
		<div id="hour-display"><?= $hour; ?></div>
	</div>

    <!-- Navbar Items-->
    <ul class="navbar-nav align-items-center ml-auto">  
 
        <?php /*
        <!-- Messages Dropdown-->
        <li class="nav-item dropdown no-caret d-none d-sm-block mr-3 dropdown-notifications">
            <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownMessages" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-envelope"></i></a>
            <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownMessages">
                <h6 class="dropdown-header dropdown-notifications-header">
                    <i class="mr-2 fas fa-envelope"></i>
                    Central de Mensagens
                </h6>

                <!-- Example Message 1  -->
                <a class="dropdown-item dropdown-notifications-item" href="#!">
                    <div class="dropdown-notifications-item-content">
                        <div class="dropdown-notifications-item-content-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                        <div class="dropdown-notifications-item-content-details">Thomas Wilcox · 58m</div>
                    </div>
                </a>
                
                <!-- Footer Link-->
                <a class="dropdown-item dropdown-notifications-footer" href="#!">Ler todas as mensagens</a>
            </div>
        </li> */?>
        <small class="mr-2">Bem vindo :) <br> <?= $user["name"] ?? ''; ?></small>
        <!-- User Dropdown-->
        <li class="nav-item dropdown no-caret mr-3 mr-lg-0 dropdown-user">
            <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownUserImage" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i></a>
            <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownUserImage">
                <h6 class="dropdown-header d-flex align-items-center">
                    <div class="dropdown-user-details">
                        <div class="dropdown-user-details-name"><?= $user["name"] ?? ''; ?></div>
                        <div class="dropdown-user-details-email"><?= $user["email"] ?? ''; ?></div>
                    </div>
                </h6>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?= $base.'/logout?token='.md5(session_id()); ?> ">
                    <div class="dropdown-item-icon"><i data-feather="log-out"></i></div>
                    Sair
                </a>
            </div>
        </li>
    </ul>
</nav>
