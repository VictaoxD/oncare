<div class="modal" id="modal-login" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Sua Sessão expirou... Faça login novamente!</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <link rel="stylesheet" href="<?= $base ?>/assets/css/login.css" />
            </div>
            <div class="modal-body">
                <!-- Login form-->
                <form id="form-modal-login" method="POST">
                    <input type="hidden" value="form-modal-login" name="id">
                    <!-- Form Group (login)-->
                    <div class="form-login form-group">
                        <label class="small mb-1" for="inputLogin">Usuário</label>
                        <input class="form-control" id="inputLogin" name="login" type="text" placeholder="Entre com seu usuário..." />
                    </div>

                    <!-- Form Group (password)-->
                    <div class="form-login form-group">
                        <label class="small mb-1" for="inputPassword">Senha</label>
                        <input class="form-control" id="inputPassword" name="senha" type="password" placeholder="Entre com a sua senha..." />
                    </div>

                    <!-- Form Group (login box)-->
                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                        <a class="small form-login" href="<?= $base; ?>/logout">Sair do sistema</a>
                        <button type="submit" class="btn btn-sm btn-default">
                            <span class="form-login">Entrar</span>
                        </button>
                    </div>
                </form>

                <div class="alert-message-modal-login mt-3"></div>
            </div>
        </div>
    </div>
</div>
