<div class="modal" id="modal-confirm" tabindex="-1" role="dialog" aria-hidden="true">
    <form
        id="form-modal-corfirm"
        class="form-submit-modal-confirm"
        method="POST" 
        data-url="" 
        data-redirect=""
    >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-confirmLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-cancel btn-light" type="button" data-dismiss="modal" onclick="resetForm('form-modal-corfirm');"></button>
                    <button class="btn btn-ok btn-danger" type="button"></button>
                </div>
            </div>
        </div>
    </form>
</div>
