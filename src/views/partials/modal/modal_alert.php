<div class="modal" id="modal-alert" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-confirmLabel"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                </div>
                <div class="message"></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-ok btn-light" data-dismiss="modal" type="button"></button>
            </div>
        </div>
    </div>
</div>
