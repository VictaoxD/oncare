<!-- ======= Footer ======= -->
<footer id="footer">
	<?php 
	/*
	<div class="footer-newsletter">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6">
					<h4>Assine nossa Newsletter</h4>
					<p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
					<form action="" method="post">
						<input type="email" name="email"><input type="submit" value="Assinar">
					</form>
				</div>
			</div>
		</div>
	</div> */
	?>

	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 footer-contact">
					<h3><span>Oncare</span></h3>
					<p>
						<strong>Email:</strong> contato@oncarebr.com.br<br>
					</p>
				</div>

				<div class="col-lg-3 col-md-6 footer-links">
					<h4>Institucional</h4>
					<ul>
						<li><i class="bx bx-chevron-right"></i> <a href="#">Perguntas Frequentes</a></li>
						<li><i class="bx bx-chevron-right"></i> <a href="#">Termos de Uso do Usuário</a></li>
						<li><i class="bx bx-chevron-right"></i> <a href="#">Termos de Uso do Psicólogo</a></li>
						<li><i class="bx bx-chevron-right"></i> <a href="#">Política de Privacidade</a></li>
					</ul>
				</div>

				<div class="col-lg-3 col-md-6 footer-links">
					<h4>Minha Conta</h4>
					<ul>
						<li><i class="bx bx-chevron-right"></i> <a href="#">Minhas Consultas</a></li>
						<li><i class="bx bx-chevron-right"></i> <a href="#">Meu Perfil</a></li>
					</ul>
				</div>

				<div class="col-lg-3 col-md-6 footer-links">
					<h4>Nossas Redes Sociais</h4>
					<p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
					<div class="social-links mt-3">
						<a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
						<a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
						<a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
						<a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="container py-4">
	<div class="copyright">
		&copy; Copyright <strong><span>Oncare 2021</span></strong>. Todos os Direitos Reservados.
	</div>
	<div class="credits" style="text-align: right;">
		<img src="<?= $base; ?>/assets/img/pagamentos.png" class="img-fluid" alt="" style="margin-top: -15px; max-width: 60%;">
	</div>
	</div>
</footer><!-- End Footer -->