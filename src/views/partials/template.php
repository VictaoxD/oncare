<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title><?= $config['config']['title']; ?></title>
        
        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="<?= $base; ?>/assets/css/styles.css" rel="stylesheet" />
        <link href="<?= $base; ?>/assets/css/styles_custom.css" rel="stylesheet" />
        <link href="<?= $base; ?>/assets/css/select2.min.css" rel="stylesheet" />
        
        <!-- Favicons -->
        <link rel="icon" type="image/x-icon" href="<?= $base; ?>/assets/img/favicon.ico" />
        <link rel="apple-touch-icon" href="<?= $base; ?>/assets/img/apple-touch-icon.png" >
        <script data-search-pseudo-elements defer src="<?= $base; ?>/assets/js/third/font-awesome-all.min.js" crossorigin="anonymous"></script>
        <script> var baseUrl = "<?= $base; ?>"; </script>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="<?= $base; ?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= $base; ?>/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
        <link href="<?= $base; ?>/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="<?= $base; ?>/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="<?= $base; ?>/assets/vendor/venobox/venobox.css" rel="stylesheet">
        <link href="<?= $base; ?>/assets/vendor/aos/aos.css" rel="stylesheet">
    </head>
    <body>
        <!-- Topo Site Informações -->
        <?php $this->render('partials/topbar', $viewData); ?>

        <!-- Header com Logo e Menu -->
        <?php $this->render('partials/header', $viewData); ?>
        
        <!-- Conteúdo da página - Dinâmico -->
        <?php $this->render($viewName, $viewData); ?>

        <!-- Rodapé -->
        <?php $this->render('partials/footer'); ?>

        <div id="preloader"></div>
        <!-- <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a> -->

        <!-- Vendor JS Files -->
        <script src="<?= $base; ?>/assets/vendor/jquery/jquery.min.js"></script>
        <script src="<?= $base; ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?= $base; ?>/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="<?= $base; ?>/assets/vendor/php-email-form/validate.js"></script>
        <script src="<?= $base; ?>/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
        <script src="<?= $base; ?>/assets/vendor/counterup/counterup.min.js"></script>
        <script src="<?= $base; ?>/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="<?= $base; ?>/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="<?= $base; ?>/assets/vendor/venobox/venobox.min.js"></script>
        <script src="<?= $base; ?>/assets/vendor/aos/aos.js"></script>
        <script>
        $('.nav-menu li').click(function() {
            $(this).siblings('li').removeClass('active');
            $(this).addClass('active');
        });
        </script>

        <!-- Template Main JS File -->
        <script src="<?= $base; ?>/assets/js/main.js"></script>
    </body>
</html>
