<?php $this->render('partials/error_partials/error_message', $viewData); ?>

<div class="card mb-4">
    <div class="card-header">
        <?= $settings["title"]; ?>
    </div>
    <form
        id="form-crud"
        class="form-submit-default" 
        data-url="<?= $settings["urlAction"]; ?>" 
        data-redirect="<?= $settings["urlRedirect"]; ?>" 
        >
        <div class="card-body">
            <?= $this->render('pages/'.$settings["view"], $viewData); ?>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-default">Salvar</button>
            <button type="button" class="btn btn-sm btn-danger loading-ajax" data-url="<?= $settings["urlRedirect"]; ?>">Cancelar</button>
        </div>
    </form>
</div>
