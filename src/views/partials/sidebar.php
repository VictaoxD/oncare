<div id="layoutSidenav_nav">
    <nav class="sidenav shadow-right sidenav-light">
        <div class="sidenav-menu">
            <div class="nav accordion" id="accordionSidenav">
               
                <!-- Dropdown - Search-->
                <div class="mt-3 ml-2 mr-2">
                    <form class="form-inline mr-auto w-100">
                        <div class="input-group input-group-joined input-group-solid">
                            <input class="form-control" id="search-sidebar" type="text" placeholder="Pesquise por..." aria-label="Search" aria-describedby="basic-addon2" />
                            <div class="input-group-append">
                                <div class="input-group-text"><i class="fas fa-search"></i></div>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="content-search" class="ml-2 mr-2">
                    <ul id="my-search">
                        <li class="display-off" id="search-not"><i class="fas fa-exclamation-circle"></i> Não foi encontrada nem uma correspondência do valor digitado!</li>
                        <?php foreach($config['menu'] as $value): ?>
                            <?php $i = 0; foreach($value["sub-menu"] as $item): ?>
                                    <li class="display-off">
                                        <a class="nav-link" href="<?php echo $base.$item["route"]; ?>" >
                                            <i class="fas fa-search"></i>  <?php echo $item["name"]; ?>                                                                            
                                        </a>
                                    </li>
                                <?php foreach($item["sub-cat"] as $subItem): ?>
                                    <li class="display-off">
                                        <a class="nav-link" href="<?php echo $base.$subItem["route"]; ?>">
                                            <i class="fas fa-search"></i>  <?php echo $subItem["name"]; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>

                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                
                <!-- Sidenav Accordion (Pages)-->
                <?php foreach($config['menu'] as $value): ?>
                    
                    <?php if(count($value["sub-menu"]) > 0): ?>
                        <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#pagina<?php echo $value["identification"]; ?>" aria-expanded="false" aria-controls="pagina<?php echo $value["identification"]; ?>">
                            <div class="nav-link-icon"><i class="<?php echo $value["icon"]; ?>"></i></div>
                            <?php echo $value["name"]; ?>
                            <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                    <?php else: ?>
                        <a class="nav-link" href="<?php echo $base.$value["route"]; ?>" >
                            <div class="nav-link-icon"><i class="<?php echo $value["icon"]; ?>"></i></div>
                            <?php echo $value["name"]; ?>
                        </a>
                    <?php endif; ?>
                    
                    <div class="collapse" id="pagina<?php echo $value["identification"]; ?>" data-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" >
                            <?php $i = 0; foreach($value["sub-menu"] as $item): ?>

                                <?php if(count($item["sub-cat"]) > 0): ?>
                                    <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#paginaItem<?php echo $value["identification"] . '-' .$i; ?>" aria-expanded="false" aria-controls="paginaItem<?php echo $value["identification"] . '-' .$i; ?>">
                                        <?php echo $item["name"]; ?>                                       
                                        <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>                                    
                                    </a>

                                    <div class="collapse" id="paginaItem<?php echo $value["identification"] . '-' .$i; ?>" >
                                        <nav class="sidenav-menu-nested nav">
                                            <?php foreach($item["sub-cat"] as $subItem): ?>
                                                <a class="nav-link" href="<?php echo $base.$subItem["route"]; ?>">
                                                    <?php echo $subItem["name"]; ?>
                                                </a>
                                            <?php endforeach; ?>
                                        </nav>
                                    </div>
                                <?php else: ?>
                                    <a class="nav-link" href="<?php echo $base.$item["route"]; ?>" >
                                        <?php echo $item["name"]; ?>                                                                            
                                    </a>
                                <?php endif; ?>

                            <?php $i++; endforeach; ?>
                        </nav> 
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
       
    </nav>
</div>
