<div class="form-group">
    <div class="col-sm-12">
        <label for="idcliente">Cliente</label>
        <select class="custom-select select2 loading-orcamento-cliente" multiple id="idcliente" name="idCliente[]" data-required="true" title="Cliente" style="width: 100%;">
            <?php foreach($clientes as $value): ?>
                <option value="<?= $value["cli_codcliente"]; ?>"><?= $value["cli_nomecliente_a"] . ' - ' . $value["cli_cadastronacional_a"]; ?></option>
            <?php endforeach; ?>
        </select>
        <small>Selecione 1 ou mais clientes</small>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idcliente">Pedidos</label>
        <select class="custom-select select2" multiple id="idPedido" name="idPedido[]" data-required="true" title="Pedido" style="width: 100%;">
           
        </select>
        <small>Selecione 1 ou mais pedidos</small>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idnome">Nome</label>
        <input type="text" 
            class="form-control"  
            data-required="true"
            name="nome" 
            id="idnome" 
            title="Nome"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idcpfCnpj">CPF/CNPJ</label>
        <input type="text" 
            class="form-control cpfCnpj"  
            data-required="true"
            name="cpfCnpj" 
            id="idcpfCnpj" 
            title="CPF/CNPJ"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idemail">E-mail</label>
        <input type="email" 
            class="form-control"  
            data-required="true"
            name="email" 
            id="idemail" 
            title="E-mail"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idcelular">Celular WhatsApp</label>
        <input type="text" 
            class="form-control celular"  
            data-required="true"
            name="celular" 
            id="idcelular" 
            title="Celular WhatsApp"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idsenha">Senha</label>
        <input type="password" 
            class="form-control"  
            data-required="true"
            name="senha" 
            id="idsenha" 
            title="Senha"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idsenha">Confirme a senha</label>
        <input type="password" 
            class="form-control"  
            data-required="true"
            name="senhaConfirme" 
            id="idsenha_confirmar" 
            title="Confirmação de senha"
        >
        <small id="confirmacao-senha" ></small>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idstatus">Status</label>
        <select class="custom-select select2" id="idstatus" name="status" title="Status" style="width: 100%;">
            <option value="">Selecione...</option>
            <option value="1">Ativo</option>
            <option value="0">Inativo</option>
        </select>
    </div>
</div>