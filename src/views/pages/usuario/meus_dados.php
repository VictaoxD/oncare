<div class="panel-meus-dados">

    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
                <label for="idnome">Nome</label>
                <input type="text" 
                    class="form-control"  
                    data-required="true"
                    name="nome" 
                    id="idnome" 
                    title="Nome"
                    value="<?= $dados['nome']; ?>"
                    <?php if($dados['nome'] != ''){ echo 'disabled=\'disabled\'';}?>
                >
            </div>
            <div class="col-sm-6">
                <label for="idemail">E-mail</label>
                <input type="text" 
                    class="form-control"  
                    data-required="true"
                    name="email" 
                    id="idemail" 
                    title="E-mail"
                    value="<?= $dados['email']; ?>"
                    disabled="disabled"
                >
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
                <label for="idcpfcnpj">CPF/CNPJ</label>
                <input type="text" 
                    class="form-control"  
                    data-required="true"
                    name="cpfcnpj" 
                    id="idcpfcnpj" 
                    title="CPF/CNPJ"
                    value="<?= $dados['cpfCnpj']; ?>"
                    <?php if($dados['cpfCnpj'] != ''){ echo 'disabled=\'disabled\'';}?>
                >
            </div>
            <div class="col-sm-6">
                <label for="idcelular">Celular</label>
                <input type="text" 
                    class="form-control"  
                    data-required="true"
                    name="celular" 
                    id="idcelular" 
                    title="Celular"
                    value="<?= $dados['celular']; ?>"
                    <?php if($dados['celular'] != ''){ echo 'disabled=\'disabled\'';}?>
                >
            </div>
        </div>
    </div>
</div>