<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th class="alignThFixes">Deletar</th>
            <th class="alignThFixes">Editar</th>
            <th class="alignThFixes">Detalhes</th>
            <?php foreach ($datatable["header"] as $value) : ?>
                <th>
                    <a href="javascript:void(0)" class="loading-ajax" data-url="<?= $base . $value["link"]; ?>" data-div="card-datatable">
                        <?= $value["title"]; ?>
                        <?php switch ($value["type"]) {
                            case 'desc':
                                echo '<i class="fas fa-sort-up"></i>';
                                break;
                            case 'asc':
                                echo '<i class="fas fa-sort-down"></i>';
                                break;
                            default:
                                echo '';
                                break;
                        } ?>
                    </a>
                </th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>

        <?php foreach ($datatable["data"] as $value) : ?>
            <tr class="lineTr">

                <td class="alignFixes">
                    <button class="btn btn-datatable btn-light btn-icon delete-ajax" data-url="<?= $settings["urlDelete"]; ?>?idKey=1" data-redirect="<?= $settings["urlRedirect"]; ?>" data-key="<?= $value[0]; ?>">
                        <i class="fas fa-trash-alt"></i>
                    </button>
                </td>
                <td class="alignFixes">
                    <button class="btn btn-datatable btn-light btn-icon loading-ajax" data-url="<?= $settings["urlEdit"]; ?>?idKey=<?= $value[0]; ?>">
                        <i class="fas fa-edit"></i>
                    </button>
                </td>
                <td class="alignFixes">
                    <button class="btn btn-datatable btn-light btn-icon loading-ajax" data-modal="true" data-url="<?= $settings["urlDetail"]; ?>?idKey=<?= $value[0]; ?>">
                        <i class="fas fa-info-circle"></i>
                    </button>
                </td>

                <?php foreach ($datatable["header"] as $k => $v) : ?>
                    <td><?= $value[$k]; ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>

    </tbody>
</table>