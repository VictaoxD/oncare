
<button class="btn btn-default btn-sm mb-2 loading-ajax" data-url="<?= $settings["urlInsert"]; ?>">
    Cadastro
</button>

<div class="card">
    <div class="card-body">

        <div class="float-left pb-1 mr-2 mb-5 align-items-center">
           <h2>Filtro</h2>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>Num Pedido</th>
                    <th>Data Pedido</th>
                    <th>Num Amostra</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="text" class="form-control form-control-sm">
                    </td>
                    <td>
                        <input type="text" class="form-control form-control-sm">
                    </td>
                    <td>
                        <input type="text" class="form-control form-control-sm">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="card mb-4 mt-2">
    <div class="card-body card-datatable">
        
        <?php $this->render('pages/usuario/datatable', $viewData); ?>    

    </div>
</div>