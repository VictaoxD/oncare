<div class="form-group">
    <div class="col-sm-12">
        <label for="idsenha">Senha</label>
        <input type="password" 
            class="form-control"  
            data-required="true"
            name="senha" 
            id="idsenha" 
            title="Senha"
            data-function="checkedPassword"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idsenha">Confirme a senha</label>
        <input type="password" 
            class="form-control"  
            data-required="true"
            name="senhaConfirme" 
            id="idsenha_confirmar" 
            title="Confirmação de senha"
            data-function="checkedPassword"
        >
    </div>
    <div class="col-sm-12">
        <small id="confirmacao-senha" ></small>
    </div>
</div>