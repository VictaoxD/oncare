<input type="hidden" value="<?= $usuario['idUsuario']; ?>" name="idUsuario">

<div class="form-group">
    <div class="col-sm-12">
        <label for="idcliente">Clientes</label>
        <select class="custom-select select2 loading-orcamento-cliente" multiple id="idcliente" name="idCliente[]" data-required="true" title="Clientes" style="width: 100%;">
            <?php foreach($clientes as $value): ?>
                <option value="<?= $value["cli_codcliente"]; ?>" <?= in_array($value["cli_codcliente"], $clienteUsuario) ? 'selected' : ''; ?>><?= $value["cli_nomecliente_a"] . ' - ' . $value["cli_cadastronacional_a"]; ?></option>
            <?php endforeach; ?>
        </select>
        <small>Selecione 1 ou mais clientes</small>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idcliente">Pedidos</label>
        <select class="custom-select select2" multiple id="idPedido" name="idPedido[]" data-required="true" title="Pedido" style="width: 100%;">
            <?php foreach($pedidos as $value): ?>
                <option value="<?= $value["cli_codcliente"].'-'.$value["orc_codorcamento"]; ?>" <?= in_array($value["orc_codorcamento"], $pedidoUsuario) ? 'selected' : ''; ?> ><?= $value["orc_codorcamento"]; ?></option>
            <?php endforeach; ?>
        </select>
        <small>Selecione 1 ou mais pedidos</small>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idnome">Nome</label>
        <input type="text" 
            class="form-control"  
            data-required="true"
            name="nome" 
            id="idnome" 
            title="Nome"
            value="<?= $usuario['nome']; ?>"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idcpfcnpj">CPF/CNPJ</label>
        <input type="text" 
            class="form-control cpfCnpj"  
            data-required="true"
            name="cpfCnpj" 
            id="idcpfcnpj" 
            title="CPF/CNPJ"
            value="<?= $usuario['cpfCnpj']; ?>"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idemail">E-mail</label>
        <input type="email" 
            class="form-control"  
            data-required="true"
            name="email" 
            id="idemail" 
            title="E-mail"
            value="<?= $usuario['email']; ?>"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idcelular">Celular WhatsApp</label>
        <input type="text" 
            class="form-control celular"  
            data-required="true"
            name="celular" 
            id="idcelular" 
            title="Celular WhatsApp"
            value="<?= $usuario["celular"]; ?>"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idsenha">Senha</label>
        <input type="password" 
            class="form-control"  
            name="senha" 
            id="idsenha" 
            title="Senha"
        >
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idsenha">Confirme a senha</label>
        <input type="password" 
            class="form-control"  
            name="senhaConfirme" 
            id="idsenha_confirmar" 
            title="Confirmação de senha"
        >
        <small id="confirmacao-senha" ></small>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <label for="idstatus">Status</label>
        <select class="custom-select select2" id="idstatus" name="status" title="Status" style="width: 100%;">
            <option value="">Selecione...</option>
            <option value="1" <?= $usuario['status'] == '1' ? 'selected' : ''; ?>>Ativo</option>
            <option value="0" <?= $usuario['status'] == '0' ? 'selected' : ''; ?>>Inativo</option>
        </select>
    </div>
</div>