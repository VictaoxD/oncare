<div class="text-center">
    <div class="error mx-auto" data-text="404">404</div>
    <p class="lead text-gray-800 mb-5">Pagina não encontrada</p>
    <a href="<?= $base; ?>">&larr; Voltar para a Home</a>
</div>