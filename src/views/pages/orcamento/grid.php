<div class="card">
    <div class="card-body">
        <form action="" method="">
            <div class="float-left pb-1 mr-2 mb-5 align-items-center">
                <h2>Filtro</h2>
            </div>

            <div class="float-right pb-1 mr-2 mb-3 align-items-center">
                <label for="idclientes">Selecione um cliente</label>
                <select class="custom-select select2 loading-orcamento" name="clientes" id="idclientes" style="width: 100%;">
                    <option value="">Selecione...</option>
                    <?php foreach($clientes as $value): ?>
                        <option value="<?= $value["cli_codcliente"]; ?>" <?= $value['cli_codcliente'] == $idCliente ? 'selected':''; ?>><?= $value["cli_nomecliente_a"] . ' - ' . $value["cli_cadastronacional_a"]; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Num Pedido</th>
                        <th>Data Pedido</th>
                        <th>Num Amostra</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" class="form-control form-control-sm">
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-sm">
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-sm">
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="form-group">
                <div class="form-row">
                    <div class="col-1">
                        <label for="idlimite">Limite</label>
                        <select class="custom-select select2" name="limite" id="idlimite" style="width: 100%;">
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="500">500</option>
                            <option value="1000">1000</option>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-default btn-sm">Pesquisar</button>

        </form>
    </div>
</div>

<div class="card mb-4 mt-2">
    <div class="card-body card-datatable">

        <?php $this->render('pages/orcamento/datatable', $viewData); ?>
        
    </div>
</div>