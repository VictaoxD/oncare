<table class="table table-striped table-hover">
    <thead>
        <tr>
            <?php foreach ($datatable["header"] as $value) : ?>
                <th>
                    <a href="javascript:void(0)" class="loading-ajax" data-url="<?= $base . $value["link"]; ?>&idCliente=<?= $idCliente; ?>" data-div="card-datatable">
                        <?= $value["title"]; ?>
                        <?php switch ($value["type"]) {
                            case 'desc':
                                echo '<i class="fas fa-sort-up"></i>';
                                break;
                            case 'asc':
                                echo '<i class="fas fa-sort-down"></i>';
                                break;
                            default:
                                echo '';
                                break;
                        } ?>
                    </a>
                </th>
            <?php endforeach; ?>
            <th>
                Pdf
            </th>
            <th>
                Excel
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($datatable["data"] as $value) : ?>
            <tr class="lineTr">
                <?php foreach ($datatable["header"] as $k => $v) : ?>
                    <td><?= $value[$k]; ?></td>
                <?php endforeach; ?>
                <td>
                    <a class="btn btn-datatable btn-light btn-sm" href="<?= $base; ?>/orcamento-pdf?idCliente=<?= $idCliente ?>&idPedido=<?= $value[0]; ?>&setor=<?= $value[6]; ?>" target="_blank">
                        <i class="fas fa-file-pdf"></i>
                    </a>
                </td>
                <td>
                    <a class="btn btn-datatable btn-light btn-sm" href="<?= $base; ?>/orcamento-excel?idCliente=<?= $idCliente ?>&idPedido=<?= $value[0]; ?>&setor=<?= $value[6]; ?>" target="_blank">
                        <i class="fas fa-file-excel"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>