<?php $this->render('partials/banner', $viewData); ?>

<main>
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
        <div class="section-title">
            <h3>Como <span>funciona.</span></h3>
        </div>
        <div class="container" data-aos="fade-up">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="col-md-12 col-lg-12 col-sm-12 d-flex align-items-stretch mb-5 mb-lg-3">
                        <div class="icon-box mr-2" data-aos="fade-up" data-aos-delay="100" style="text-align: center;">
                            <div class="icon"><img src="<?= $base; ?>/assets/img/3-removebg-preview_bg.png" class="img1"></div>
                            <h4 class="title"><a href="">Primeiro passo</a></h4>
                            <p class="description">Escolha um psicólogo que se sinta mais à vontade.</p>
                        </div>
                        <div class="icon-box ml-1" data-aos="fade-up" data-aos-delay="100" style="text-align: center;">
                            <div class="icon"><img src="<?= $base; ?>/assets/img/3-removebg-preview_bg.png" class="img2"></i></div>
                            <h4 class="title"><a href="">Segundo passo</a></h4>
                            <p class="description">Escolha a data e hora de sua consulta na agenda do psicólogo.</p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12 d-flex align-items-stretch mb-5 mb-lg-3">
                        <div class="icon-box mr-2" data-aos="fade-up" data-aos-delay="200" style="text-align: center;">
                            <div class="icon"><img src="<?= $base; ?>/assets/img/3-removebg-preview_bg.png" class="img3"></div>
                            <h4 class="title"><a href="">Terceiro passo</a></h4>
                            <p class="description">Conclua o pagamento com toda segurança e praticidade.</p>
                        </div>
                        <div class="icon-box ml-1" data-aos="fade-up" data-aos-delay="200" style="text-align: center;">
                            <div class="icon"><img src="<?= $base; ?>/assets/img/3-removebg-preview_bg.png" class="img4"></div>
                            <h4 class="title"><a href="">Quarto passo</a></h4>
                            <p class="description">Prepare-se para uma grande experiência para sua saúde e bem-estar.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6" data-aos="zoom-out" data-aos-delay="300" style="border-left: 1px solid #000;">
                    <img src="<?= $base; ?>/assets/img/about.jpg" class="img-fluid" alt="" style="padding: 30px;">
                </div>
            </div>
        </div>
    </section><!-- End Featured Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h3>Alguns de nossos <span>profissionais.</span></h3>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bxl-dribbble"></i></div>
                        <h4><a href="">Lorem Ipsum</a></h4>
                        <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
                        <br/>
                        <a href="#about" class="btn-padrao scrollto">Agendar</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-file"></i></div>
                        <h4><a href="">Sed ut perspiciatis</a></h4>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                        <br/>
                        <a href="#about" class="btn-padrao scrollto">Agendar</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-tachometer"></i></div>
                        <h4><a href="">Magni Dolores</a></h4>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
                        <br/>
                        <a href="#about" class="btn-padrao scrollto">Agendar</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-world"></i></div>
                        <h4><a href="">Nemo Enim</a></h4>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
                        <br/>
                        <a href="#about" class="btn-padrao scrollto">Agendar</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="200">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-slideshow"></i></div>
                        <h4><a href="">Dele cardo</a></h4>
                        <p>Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur</p>
                        <br/>
                        <a href="#about" class="btn-padrao scrollto">Agendar</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="300">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-arch"></i></div>
                        <h4><a href="">Divera don</a></h4>
                        <p>Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur</p>
                        <br/>
                        <a href="#about" class="btn-padrao scrollto">Agendar</a>
                    </div>
                </div>
            </div>
            <div style="text-align: center; padding-top: 60px;">
                <a href="#" class="btn-padrao scrollto" style="width: 270px;">Ver Mais</a>
            </div>
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
    <div class="container" data-aos="zoom-in">
        <div class="section-title">
            <h3 style="color: #fff;">Veja o que os pacientes falam sobre a Oncare.</h3>
        </div>
        <div class="owl-carousel testimonials-carousel">
            <div class="testimonial-item">
                <img src="<?= $base; ?>/assets/img/9.png" class="testimonial-img" alt="">
                <h3>Saulo Godoy</h3>
                <h4>Eletricista</h4>
                <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
            </div>
            <div class="testimonial-item">
                <img src="<?= $base; ?>/assets/img/9.png" class="testimonial-img" alt="">
                <h3>Sara Santos Alves</h3>
                <h4>Designer</h4>
                <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
            </div>
            <div class="testimonial-item">
                <img src="<?= $base; ?>/assets/img/9.png" class="testimonial-img" alt="">
                <h3>Janaína Mattos da Silva</h3>
                <h4>Cabelereira</h4>
                <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
            </div>
            <div class="testimonial-item">
                <img src="<?= $base; ?>/assets/img/9.png" class="testimonial-img" alt="">
                <h3>Matheus Henrique Ferreira</h3>
                <h4>Agrônomo</h4>
                <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
            </div>
            <div class="testimonial-item">
                <img src="<?= $base; ?>/assets/img/9.png" class="testimonial-img" alt="">
                <h3>John Wesley Barboza</h3>
                <h4>Estudante</h4>
                <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
            </div>
        </div>

    </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h3>Benefícios por utilizar a <span>Oncare.</span></h3>
        </div>

        <ul class="faq-list" data-aos="fade-up" data-aos-delay="100">
            <li>
                <a data-toggle="collapse" class="" href="#faq1">1 - Variedade de psicólogos e especialidades <i class="icofont-simple-up"></i></a>
                <div id="faq1" class="collapse show" data-parent=".faq-list">
                <p>
                    Aqui você encontra diversos profissionais altamente selecionados e qualificados para cuidar de você. Diversas especialidades, pós-graduações, linhas de pensamento e muito mais!
                </p>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#faq2" class="collapsed">2 - Horários flexíveis <i class="icofont-simple-up"></i></a>
                <div id="faq2" class="collapse" data-parent=".faq-list">
                <p>
                    Hoje os psicólogos Oncare possuem agendas que se adequam às suas necessidades. Seja bem cedinho, no almoço, à noite ou finais de semana, estaremos lá para te atender.
                </p>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#faq3" class="collapsed">3 - Atendimento humanizado <i class="icofont-simple-up"></i></a>
                <div id="faq3" class="collapse" data-parent=".faq-list">
                <p>
                    Desde sua primeira consulta, uma pessoa de nosso suporte ficará responsável pela sua satisfação e bom atendimento. Na Oncare, sempre queremos que sua experiência seja a melhor possível.
                </p>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#faq4" class="collapsed">4 - Reembolso dos planos de saúde <i class="icofont-simple-up"></i></a>
                <div id="faq4" class="collapse" data-parent=".faq-list">
                <p>
                    Muitos pacientes da Oncare hoje conseguem reembolso de suas consultas feitas. Entre em contato com seu plano e veja a possibilidade.
                </p>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#faq5" class="collapsed">5 - Praticidade na hora da consulta <i class="icofont-simple-up"></i></a>
                <div id="faq5" class="collapse" data-parent=".faq-list">
                <p>
                    Não é necessário se deslocar até consultórios ou clínicas. Agora, você pode fazer terapia do conforto de sua casa!
                </p>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#faq6" class="collapsed">6 - Segurança e privacidade <i class="icofont-simple-up"></i></a>
                <div id="faq6" class="collapse" data-parent=".faq-list">
                <p>
                    Plataforma segura e com privacidade do início ao fim. Atualmente contamos com todos, e mais modernos, processos necessários de segurança e discrição de nossos pacientes.
                </p>
                </div>
            </li>
        </ul>
    </div>
    </section><!-- End Frequently Asked Questions Section -->
</main>