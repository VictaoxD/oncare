<main>
    <br/><br/><br/><br/>
    <!-- ======= Login Psicologo Area ======= -->
    <section id="login" class="login section-bg">
        <div class="container" data-aos="fade-up">
            <div class="row">
                <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="100">
                    <form action="forms/login.php" method="post" role="form" class="php-email-form">
                        <div class="form-row">
                            <div class="col form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="E-mail:" data-rule="email" data-msg="Por favor, insira um e-mail válido" />
                                <div class="validate"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col form-group">
                                <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha:" data-rule="password" data-msg="Por favor, insira uma senha válida" />
                                <div class="validate"></div>
                            </div>
                        </div>
                        <div class="text-center"><button type="submit">Entrar</button></div>
                        <div class="text-center"><small>Não possui Conta? <a href="#">Crie agora!</a></small></div>
                    </form>
                </div>
                <div class="col-lg-6" data-aos="zoom-out" data-aos-delay="100">
                    <img src="assets/img/about.jpg" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section><!-- End Login Psicologo Area -->
</main>