<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>CONAG</title>
        <link href="<?= $base; ?>/assets/css/styles.css" rel="stylesheet" />
        <link href="<?= $base; ?>/assets/css/login.css" rel="stylesheet" />
        <link rel="icon" type="image/x-icon" href="<?= $base; ?>/assets/img/favicon.ico" />
        <script data-search-pseudo-elements defer src="<?= $base; ?>/assets/js/third/font-awesome-all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-conag">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-4">
                                <!-- Basic login form-->
                                <div class="card shadow-lg border-0 rounded-lg mt-5">

                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="px-3 text-center">
                                                <img class="img-fluid " src="<?= $base; ?>/assets/img/logoRibersolo.png" alt="Logo Ribersolo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-header justify-content-center"><h3 class="font-weight-light my-4">{MENSAGEM EDITAVEL}</h3></div>

                                    <div class="card-body">
                                    <div class="text-center">
                                        <h3>Email enviado com Sucesso!</h3>
                                    </div>
                                    </div>

                                    <div class="card-footer text-center">
                                        <strong>CONAG - Gestão Online | <a href="https://www.conag.com.br" target="_blank">www.conag.com.br</a></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer mt-auto footer-dark">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 small">Copyright &copy; <a href="https://www.conag.com.br/" target="_blank">CONAG</a> <?php echo date('Y'); ?></div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="<?= $base; ?>/assets/js/third/jquery-3.5.1.min.js" ></script>
        <script src="<?= $base; ?>/assets/js/third/bootstrap.bundle.min.js" ></script>
        <script src="<?= $base; ?>/assets/js/scripts.js"></script>
        <script src="<?= $base; ?>/assets/js/login.js"></script>
    </body>
</html>
