<?php
namespace core;

use \core\RouterBase;

class Router extends RouterBase {
    public $routes;

    public function get($endpoint, $trigger, $security_action = array()) {
        $this->routes['get'][$endpoint] = array($trigger, $security_action);
    }

    public function post($endpoint, $trigger, $security_action = array()) {
        $this->routes['post'][$endpoint] = array($trigger, $security_action);
    }

    public function put($endpoint, $trigger) {
        $this->routes['put'][$endpoint] = $trigger;
    }

    public function delete($endpoint, $trigger) {
        $this->routes['delete'][$endpoint] = $trigger;
    }

}