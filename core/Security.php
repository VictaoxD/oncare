<?php
namespace core;

class Security {
    const LOGADO_LIBERAR = "logado_liberar";
    const SEMPRE_LIBERAR = "sempre_liberar";

    private static function get_class_constants()
    {
        $reflect = new \ReflectionClass(__CLASS__);
        return $reflect->getConstants();
    }

    public static function isAValidAction(string $action)
    {
        $action_var = strtoupper($action);
        if (self::get_class_constants()[$action_var] == $action) {
            return true;
        } else {
            return false;
        }
    }
}
