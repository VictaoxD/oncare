<?php
namespace core;

use \src\Config;

class RouterBase {
    public function run($routes) {
        global $middleware;

        $method = Request::getMethod();
        $url = Request::getUrl();

        // Define os itens padrão
        $path = Config::PATH_CONTROLLER;
        $controller = Config::ERROR_CONTROLLER;
        $action = Config::DEFAULT_ACTION;
        $args = [];
        $route = "";
        $security = array();

        if(isset($routes[$method])) {
            foreach($routes[$method] as $route => $packing) {
                $callback = $packing[0];
                $security = $packing[1];

                // Identifica os argumentos e substitui por regex
                $pattern = preg_replace('(\{[a-z0-9]{1,}\})', '([a-z0-9-]{1,})', $route);

                // Faz o match da URL
                if(preg_match('#^('.$pattern.')*$#i', $url, $matches) === 1) {
                    array_shift($matches);
                    array_shift($matches);

                    // Pega todos os argumentos para associar
                    $itens = array();
                    if(preg_match_all('(\{[a-z0-9]{1,}\})', $route, $m)) {
                        $itens = preg_replace('(\{|\})', '', $m[0]);
                    }

                    // Faz a associação
                    $args = array();
                    foreach($matches as $key => $match) {
                        $args[$itens[$key]] = $match;
                    }

                    // Seta o controller/action
                    $callbackSplit = explode('@', $callback);
                    $path = $callbackSplit[0];
                    $controller = $callbackSplit[1];
                    if(isset($callbackSplit[2])) {
                        $action = $callbackSplit[2];
                    }

                    break;
                }
            }
        }
        
        $controller = "\src\controllers\\$path\\$controller";
        $definedController = new $controller();
        $middleware->runController($route, $security, $definedController, $action, $args);
    }
    
}