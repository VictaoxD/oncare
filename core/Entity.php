<?php
namespace core;

use src\helpers\FunctionHelper;
use ReflectionException;
use ReflectionObject;
use ReflectionProperty;
use src\helpers\ErrorHelper;
use src\helpers\SanitizeHelper;

class Entity {

    private $error = [];

    // Variables used by SanitizeHelper()
    // They should be override inside child class
    //
    // validations = ['key' => ['keyRule' => 'Rule message']]
    const validations = [];
    // sanitize = ['key' => 'rule']
    const sanitize = [];
    // required = ['key' => 'rule']
    const required = [];

    public function __construct($values = null)
    {
        $this->loadFromArray($values);
    }

    private function loadFromArray($values)
    {
        //verifica se é um array e nao esta vazio
        if (!$values || !is_array($values)) {
            return;
        }

        //instancia um objeto da classe 
        $reflection = new ReflectionObject($this);

        //percorre todos os elementos
        foreach ($values as $key => $value) {
            //cria o metono set com a primeira letra da key upperCase
            $methodName = 'set' . ucwords($key);

            try {
                //se existir o metodo set e nao tiver erros adiciona o dado
                if($reflection->hasMethod($methodName) && count($this->error) == 0){
                    $method = $reflection->getMethod($methodName);
                    $method->invoke($this, $value);
                }
            } catch (ReflectionException $e) {
                echo $e;
            }
        }

        $this->showErrorIfExist('Validação de formulario!');
    }

    private function addError($nameFiled, $message){
        $item = [
            "input" => $nameFiled,
            "message" => $message
        ];

        array_push($this->error, $item);
        return;
    }

    private function showErrorIfExist($title)
    {
        if (count($this->error) > 0) {
            ErrorHelper::setSessionError([
                    "status" => false,
                    'title' => $title,
                    'color' => 'alert-danger',
                ],
                $this->error
            );
            return true;
        }
        return false;
    }

    public function validateFields($selectedFields = [])
    {
        $reflection = new ReflectionObject($this);
        $validator = new SanitizeHelper(static::validations, static::sanitize);

        if (!empty($selectedFields)) {
            foreach ($selectedFields as $key => $value) {
                $k = str_replace(':', '', $key);
                if ($reflection->hasProperty($k)) {
                    $value = $validator->sanitize($value, static::sanitize[$k] ?? []);

                    $rules = static::validations[$k] ?? [];
                    foreach ($rules as $rule => $message) {
                        if (!$validator->validate($value, $rule)) {
                            $this->addError($k, $message);
                        }
                    }
                } else {
                    $this->addError($k, "Chave '".$k."' usada não existe");
                }
            }
        }

        return !$this->showErrorIfExist("Validação dos Campos");
    }
}
