<?php
namespace core;

use src\api\JsonAjax;
use \src\Config;
use src\helpers\ConfigHelper;
use src\helpers\FunctionHelper;
use src\helpers\LoginHelper;
use src\helpers\MenuHelper;

class Controller {

    const CAMINHO = false;
    const CLASSEDAO = false;

    private static $ob_control = false;

    public function redirect($url) {
        $url = $this->getBaseUrl().$url;
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }

    public function getBaseUrl() {
        $base = (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') ? 'https://' : 'http://';
        $base .= $_SERVER['SERVER_NAME'];
        if($_SERVER['SERVER_PORT'] != '80') {
            $base .= ':'.$_SERVER['SERVER_PORT'];
        }
        $base .= Config::BASE_DIR;

        return $base;
    }

    private static function ob_open()
    {
        if (!self::$ob_control) {
            self::$ob_control = ob_start();
            return true;
        }
        return false;
    }

    private static function ob_close()
    {
        $dump = ob_get_contents();
        ob_end_clean();
        self::$ob_control = false;
        return $dump;
    }

    private function _render($viewName, $viewData)
    {
        if (file_exists('../src/views/'.$viewName.'.php')) {
            extract($viewData);
            $base = $this->getBaseUrl();
            require '../src/views/'.$viewName.'.php';
        }
    }

    private function _renderAjax($viewName, $viewData)
    {
        $ajax_entrypoint = self::ob_open();

        $this->_render($viewName, $viewData);

        if ($ajax_entrypoint) {
            // This entrypoint control handles recursive calls
            JsonAjax::responseHTML(self::ob_close());
        }
    }

    public function render($viewName, $viewData = []) {
        if (Request::isAjax()) {
            $this->_renderAjax($viewName, $viewData);
        } else {
            $this->_render($viewName, $viewData);
        }
    }

    public function renderTemplate($viewName, $viewData = [])
    {
        if (Request::isAjax()) {
            $this->_renderAjax($viewName, $viewData);
        }

        $user = LoginHelper::getInfoUser();

        //carrega os menus de acordo com a permissao do usuario
        $viewData["config"] = ConfigHelper::getConfig();
        $viewData["date"] = FunctionHelper::dateHourEUAtoBRA(FunctionHelper::currentDate());
        $viewData["hour"] = FunctionHelper::currentHour();
        /* $viewData["user"] = [
            "email" => $user->getEmail(),
            "name" => $user->getNome(),
        ]; */

        extract($viewData);
        $base = $this->getBaseUrl();
        require '../src/views/partials/template.php';
    }

    public function transactionsWithoutErrors($model)
    {
        if (JsonAjax::hasError()) {
            $model->rollBack();
            JsonAjax::responseErrorIfExist('Desculpe, ocorreu um erro!');
            // Return false is not necessary here because response finishes the request
        }
        return true;
    }
}
