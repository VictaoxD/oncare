<?php
namespace core;

use \core\Database;
use \PDOException;
use src\api\JsonAjax;
use src\helpers\ErrorHelper;

class Model {

    private $db;
	private $error = [];

	public function __construct() {
		$this->db = Database::getInstance();
	}

	public function insertModel($query, array $params = null, $entity = null){

		if($entity != null && !$entity->validateFields($params)){
			return false;
		}

		$stm = $this->db->prepare($query);

		if(@count($params) > 0){
			foreach($params as $key =>$value){
				$stm->bindValue($key, $value);		   
			}
		}

		try{
			return $stm->execute();
		}catch(PDOException $e){
			$this->setMessageError($stm->errorInfo(), $e);
            return false;
		}
	}

	public function updateModel($query, array $params = null, $entity = null){
		if($entity != null && !$entity->validateFields($params)){
			return false;
		}

		$stm = $this->db->prepare($query);

		if(@count($params) > 0){
			foreach($params as $key =>$value){
				$stm->bindValue($key, $value);		   
			}
		}

		try{
			return $stm->execute();

		}catch(PDOException $e){
			$this->setMessageError($stm->errorInfo(), $e);
            return false;
		}
	}

	public function deleteModel($query, array $params = null, $entity = null){
		if($entity != null && !$entity->validateFields($params)){
			return false;
		}
		
		$stm = $this->db->prepare($query);

		if(@count($params) > 0){
			foreach($params as $key =>$value){
				$stm->bindValue($key, $value);		   
			}
		}

		try{
			return $stm->execute();
		}catch(PDOException $e){
			$this->setMessageError($stm->errorInfo(), $e);
            return false;
		}
	}

	public function selectAllModel($query, array $params = null){
		$array = array();
		$stm = $this->db->prepare($query);

		if(@count($params) > 0){
			foreach($params as $key => $value){
				$stm->bindValue($key, $value);
			}
		}

		$stm->execute();

		if($stm->rowCount() > 0){
			$array = $stm->fetchAll();
		}else{
			$array = [];
		}

		return $array;
	}

	public function selectModel($query, array $params = null){
		$array = array();
		$stm = $this->db->prepare($query);

		if(@count($params) > 0){
			foreach($params as $key => $value){
				$stm->bindValue($key, $value);
			}
		}

		$stm->execute();

		if($stm->rowCount() > 0){
			$array = $stm->fetch();
		}

		return $array;
	}

	private function setMessageError($error, $exception = null){
		$code = $error[1];
		$message = $error[2];
		
		switch($code){
			case 1062:
				$message = sprintf('Valor duplicado - %s', $message);
			break;
			case 1451:
				$message = sprintf('O registro não pode ser excluido pois possui vinculo ativos.<br/>');
			break;
			default:
				if ($code == null) {
					$code = $error[0];
				}
				$message = sprintf('Ops.. ocorreu um erro - %s - %s  ', $code ,$message);
		}

		JsonAjax::appendError($message);
	}

	public function lastId(){
		return $this->db->lastInsertId();
	}

	public function startTransaction(){
		$this->db->beginTransaction();
	}

	public function commit(){
		$this->db->commit();
		$this->closedConextion();
	}

	public function rollBack(){
		$this->db->rollBack();
		$this->closedConextion();
	}

	public function closedConextion(){
		Database::closedInstance();
	}

}