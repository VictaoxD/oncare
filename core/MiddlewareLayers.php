<?php
include 'Middleware.php';
use \core\Request;
use core\Security;
use \src\helpers\LoginHelper;
use \src\Config;

// Put global to avoid many DB queries because most layers use it
$usuario = LoginHelper::getInfoUser();

class AuthRequiredMiddleware implements LayerInterface {

    public function _call($object, Closure $next)
    {
        global $usuario;

        if (!in_array(Security::SEMPRE_LIBERAR, $object->security)) {
            if (!LoginHelper::isAuthenticatedUser()) {
                if (isset($_SESSION['session_timeout_login']) && Request::isAjax()) {
                    // SessionTimeout threshold reached
                    die(header("HTTP/1.0 419 Authentication Timeout"));
                } else {
                    $object->controller->redirect('/login');
                }
            }

            if ($usuario && ($usuario->getStatus() === '0')) {
                LoginHelper::doLogout(false);
                $_SESSION['msg_flash'] = array(
                    'msg' => 'Usuario Inativo',
                    'tipo' => 'alert-danger'
                );
                $object->controller->redirect('/login');
            }
        }

        $response = $next($object);

        return $response;
    }
}

class SessionTimeoutMiddleware implements LayerInterface {

    private $session_timeout_sec;

    public function __construct()
    {
       if (defined('src\Config::SESSION_TIMEOUT_MIN')) {
            $this->session_timeout_sec = Config::SESSION_TIMEOUT_MIN * 60;
        } else {
            $this->session_timeout_sec = 15*60;
        }
    }

    public function _call($object, Closure $next)
    {
        global $usuario;

        if (LoginHelper::isAuthenticatedUser()) {
            $now = time();
            if (!isset($_SESSION['session_timeout_last'])) {
                $_SESSION['session_timeout_last'] = $now;
            } else {
                $delta = $now - $_SESSION['session_timeout_last'];
                if ($delta > $this->session_timeout_sec) {
                    LoginHelper::doLogout(false);
                    $_SESSION['session_timeout_login'] = $usuario->getIdusuario();
                } else {
                    $_SESSION['session_timeout_last'] = $now;
                }
            }
        }
        return $next($object);
    }
}

$middleware = new Middleware();

$middleware->appendLayer(new SessionTimeoutMiddleware());
$middleware->appendLayer(new AuthRequiredMiddleware());
