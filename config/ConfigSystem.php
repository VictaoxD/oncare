<?php

namespace config;

class ConfigSystem
{
    const CONFIG = [
        "title" => "Oncare",
        "logo" => [
            "path" => "/assets/img/logoOncare.png",
            "width" => "143px",
            "height" => "45px",
            "alt" => "Logo Oncare"
        ]
    ];
    
    const MENU = [
        [
            "identification" => "perfil",
            "name" => "Meu Perfil",
            "icon" => "fas fa-user",
            "route" => "",
            "active" => false,
            "sub-menu"  => [
                [
                    "name" => "Alterar Senha",
                    "route" => "/usuario-alterar-senha",
                    "active" => false,
                    "sub-cat" => []
                ],
                [
                    "name" => "Dados Cliente",
                    "route" => "/usuario-dados-cliente",
                    "active" => false,
                    "sub-cat" => []
                ],
                [
                    "name" => "Meus Dados",
                    "route" => "/usuario-meus-dados",
                    "active" => false,
                    "sub-cat" => []
                ]
            ]
        ],
        [
            "identification" => "usuario",
            "name" => "Usuários",
            "icon" => "fas fa-user-friends",
            "route" => "/usuario",
            "active" => false,
            "sub-menu"  => []
        ],
        [
            "identification" => "psicologos",
            "name" => "Psicologos",
            "icon" => "fas fa-clipboard",
            "route" => "/psicologos",
            "active" => false,
            "sub-menu"  => []
        ]
    ];

    const DATATABLE = [
        "psicologo" => [
            "config" => [
                "entity" => "Psicologo"
            ],
            "datatable" => [
                [
                    "field" => "psicologo.Orc_CodOrcamento",
                    "title" => "Cod Orçamento",
                    "link" => "/psicologo",
                ],
                [
                    "field" => "psicologo.orc_dataconfirmacao_d",
                    "title" => "Data Confirmação",
                    "link" => "/psicologo",
                ],
                [
                    "field" => "psicologo.DATASAIDA",
                    "title" => "Data Saida",
                    "link" => "/psicologo",
                ],
                [
                    "field" => "cliente.cli_nomecliente_a",
                    "title" => "Cliente",
                    "link" => "/orcamento",
                ],
                [
                    "field" => "endfazenda.Cidade_a",
                    "title" => "Cidade",
                    "link" => "/orcamento",
                ],
                [
                    "field" => "endfazenda.Nome_a",
                    "title" => "Local",
                    "link" => "/orcamento",
                ],
                [
                    "field" => "CONCAT(setor.set_codsetor, ' - ', setor.set_setor_a) AS set_setor_a",
                    "title" => "Setor",
                    "link" => "/orcamento",
                ],
                [
                    "field" => "COUNT(DISTINCT amostra.Amo_CodAmostra) as qtidadeamostra",
                    "title" => "Total Amostra",
                    "link" => "/orcamento",
                ],
            ]
        ],
        "usuario" => [
            "config" => [
                "entity" => "Usuario"
            ],
            "datatable" => [
                [
                    "field" => "usuario.idUsuario",
                    "title" => "Id",
                    "link" => "/usuario",
                ],
                [
                    "field" => "usuario.nome",
                    "title" => "Nome",
                    "link" => "/usuario",
                ],
                [
                    "field" => "usuario.cpfCnpj",
                    "title" => "Cpf/Cnpj",
                    "link" => "/usuario",
                ],
            ]
        ]
    ];
}